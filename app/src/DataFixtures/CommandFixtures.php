<?php

namespace App\DataFixtures;

use App\Entity\Addresses;
use App\Entity\Command;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommandFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $users = $manager->getRepository(User::class)->findAll();
        $deliveryMans = array_filter($users, function ($user) {
            return in_array('ROLE_DELIVERY',$user->getRoles());
        });
        $status = [Command::PAID_STATUS, Command::DISPATCHED_STATUS, Command::DELIVERED_STATUS];
        $addresses = $manager->getRepository(Addresses::class)->findAll();

        for ($i = 0; $i < 20; $i++) {
            $command = (new Command())
                ->setOwner($users[array_rand($users)])
                ->setStatus($status[array_rand($status)])
                ->setAddress($addresses[array_rand($addresses)]);
            ;

            switch ($command->getStatus()) {
                case Command::DISPATCHED_STATUS:
                    $command->setDeliveryMan($deliveryMans[array_rand($deliveryMans)]);
                    break;
                case Command::DELIVERED_STATUS:
                    $command->setDeliveryMan($deliveryMans[array_rand($deliveryMans)]);
                    $command->setDeliveredAt($faker->dateTime());
                    break;
            }

            $manager->persist($command);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AddresseFixtures::class
        ];
    }

}
