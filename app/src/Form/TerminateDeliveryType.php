<?php

namespace App\Form;

use App\Entity\Command;
use App\Entity\User;
use PhpParser\Node\Scalar\String_;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TerminateDeliveryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add(
            'status', HiddenType::class, [
                'empty_data' => Command::DELIVERED_STATUS
            ]
        )
            // ->add('delivery_man', HiddenType::class, [
            //     'empty_data' => $options['delivery_man']
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
            // 'delivery_man' => new User()
        ]);

        // $resolver->setAllowedTypes('delivery_man', User::class);
    }
}
