<?php

namespace App\Controller\Front;

use App\Entity\Command;
use App\Form\TakeCommandType;
use App\Form\TerminateDeliveryType;
use App\Repository\CommandRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Security\Voter\DeliveryVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DeliveryController extends AbstractController
{
    /**
     * @Route("/delivery", name="delivery")
     */
    public function index(CommandRepository $commandRepository): Response
    {
        $commands = array_map(function(Command $command){
            return [
                'form' => $this->createForm(TakeCommandType::class, $command, [
                    'delivery_man' => $this->getUser(),
                    'action' => $this->generateUrl('delivery_take', ['id' => $command->getId()])
                ])
                ->createView(),
                'command' => $command
            ];
        }, $commandRepository->findCommandWithoutDeliveryMan());

        $commandsToDelivery = array_map(function(Command $command){
            return [
                'form' => $this->createForm(TerminateDeliveryType::class, $command, [
                    'action' => $this->generateUrl('delivery_validate', ['id' => $command->getId()])
                ])
                ->createView(),
                'command' => $command
            ];
        }, $commandRepository->findDispatchedByDeliveryMan($this->getUser()->getId()));

        return $this->render('front/delivery/index.html.twig', [
            'commands' => $commands,
            'commandsToDelivery' => $commandsToDelivery,
        ]);
    }

    /**
     * @Route("/delivery/{id}/take", name="delivery_take", methods={"POST"})
     * @IsGranted(DeliveryVoter::TAKE, subject="command")
     */
    public function takeCommand(Request $request, Command $command)
    {
        $form = $this->createForm(TakeCommandType::class, $command, [
            'delivery_man' => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command->setStatus(Command::DISPATCHED_STATUS);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La commande a bien été pris en charge!');
        }
        return $this->redirectToRoute('delivery');
    }

     /**
     * @Route("/delivery/{id}", name="delivery_show")
     * @IsGranted(DeliveryVoter::VIEW, subject="command")
     * @param Command $command
     */
    public function showDelivery(Command $command)
    {
        $form =  $this->createForm(TerminateDeliveryType::class, $command, [
            'action' => $this->generateUrl('delivery_validate', ['id' => $command->getId()])
        ])
        ->createView();

        return $this->render('front/delivery/show.html.twig', [
            'command' => $command,
            'form' => $form
        ]);
    }

    /**
     * @Route("/delivery/{id}/validate", name="delivery_validate", methods={"POST"})
     * @IsGranted(DeliveryVoter::VALID, subject="command")
     */
    public function validateCommand(Request $request, Command $command)
    {
        $form = $this->createForm(TerminateDeliveryType::class, $command);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command->setStatus(Command::DELIVERED_STATUS);
            $command->setDeliveredAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La commande a bien été enregistré comme livré!');
        }
        return $this->redirectToRoute('delivery');
    }
}
