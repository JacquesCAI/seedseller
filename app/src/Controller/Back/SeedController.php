<?php

namespace App\Controller\Back;

use App\Entity\Seed;
use App\Form\SeedType;
use App\Repository\CategoryRepository;
use App\Repository\MarketRepository;
use App\Repository\SeedRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Security\Voter\SeedVoter;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/seed")
 */
class SeedController extends AbstractController
{
    /**
     * @Route("/", name="seed_index", methods={"GET"})
     * @param SeedRepository $seedRepository
     * @return Response
     */
    public function index(SeedRepository $seedRepository, CategoryRepository $categoryRepository): Response
    {
        return $this->render('back/seed/index.html.twig', [
            'seeds' => $seedRepository->findAll(),
            'categories' => $categoryRepository->findAll(),
            'backoffice' => true
        ]);
    }

    /**
     * @Route("/{id}/more-information", name="seed_more_information", methods={"GET"})
     * @param Seed $seed
     * @return Response
     */
    public function moreInformation(Seed $seed): Response
    {
        return $this->render('back/seed/more_information.html.twig', [
            'seed' => $seed,
            'backoffice' => true
        ]);
    }

    /**
     * @Route("/new", name="seed_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $seed = new Seed();
        $form = $this->createForm(SeedType::class, $seed);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seed);
            $entityManager->flush();

            return $this->redirectToRoute('seed_index');
        }

        return $this->render('front/seed/new.html.twig', [
            'seed' => $seed,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seed_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Seed $seed
     * @IsGranted(SeedVoter::EDIT, subject="seed")
     * @return Response
     */
    public function edit(Request $request, Seed $seed): Response
    {
        $form = $this->createForm(SeedType::class, $seed);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La graine `'. $seed->getName() .'` a bien été modifié!');

            return $this->redirectToRoute('admin_seed_index');
        }

        return $this->render('back/seed/edit.html.twig', [
            'seed' => $seed,
            'form' => $form->createView(),
            'backoffice' => true
        ]);
    }

    /**
     * @Route("/{id}", name="seed_delete", methods={"DELETE"})
     * @IsGranted(subject="seed")
     * @param Request $request
     * @param Seed $seed
     * @return Response
     */
    public function delete(Request $request, Seed $seed): Response
    {
        if ($this->isCsrfTokenValid('delete'.$seed->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($seed);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_seed_index');
    }
}
