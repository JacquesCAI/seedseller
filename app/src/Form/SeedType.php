<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Seed;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SeedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('imageFile', VichImageType::class, [
                "allow_delete" => true,
                "download_uri" => true,
                "image_uri" => true,
                "required" => false,
            ])
            ->add('measurementUnit')
            ->add('category', EntityType::class, [
                "class" => Category::class,
                "choice_label" => function ($category) {
                    return $category->getName();
                }
            ])
            ->add('description')
            ->add('additionalInformation')
            ->add('lifting', null, ['label'=>false])
            ->add('numberSeedsPerGram')
            ->add('plantingDensity')
            ->add('sowingdate')
            ->add('harvestdate')
            ->add('cultivationPractice')
            ->add('diseasesAndMainPests')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Seed::class,
        ]);
    }
}
