<?php

namespace App\Form;

use App\Entity\Seed;
use App\Entity\Market;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seed', HiddenType::class, [
                'empty_data' => $options['seed']
            ])
            ->add('owner', HiddenType::class, [
                'empty_data' => $options['owner']
            ])
            ->add('quantity', NumberType::class, [
                'required' => true
            ])
            ->add('price', NumberType::class, [
                'required' => true
            ])
            ->add('canSeparate', CheckboxType::class, [
                'label_attr' => ['class' => 'switch-custom'],
                'required' => false
            ])
            ->add('minimumQuantity',NumberType::class, [
                'attr' => ['autocomplete' => 'off'],
                'label' => 'Quantité minimum',
                'required' => false
            ] )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Market::class,
            'seed' => new Seed(),
            'owner' => new User()
        ]);

        $resolver
            ->setAllowedTypes('seed', Seed::class)
            ->setAllowedTypes('owner', User::class);
    }
}
