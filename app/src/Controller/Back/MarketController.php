<?php

namespace App\Controller\Back;

use App\Entity\Market;
use App\Form\MarketType;
use App\Repository\MarketRepository;
use App\Repository\SeedRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Security\Voter\MarketVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/market")
 */
class MarketController extends AbstractController
{
    /**
     * @Route("/list/{seed}/{user}", name="market_index", methods={"GET"})
     * @param MarketRepository $marketRepository
     * @return Response
     */
    public function index(int $seed = null, int $user = null, MarketRepository $marketRepository, UserRepository $userRepository, SeedRepository $seedRepository): Response
    {
        if ($seed !== null) {
            $seed = $seed == 0 ? null : $seedRepository->find($seed);
        }
        if ($user !== null) {
            $user = $user == 0 ? null : $userRepository->find($user);
        }

        $markets = $marketRepository->findBySeedAndOwner($seed != null ? $seed->getId() : null, $user != null ? $user->getId() : null);

        $soldMarkets = $marketRepository->findBy([
            'quantity' => 0
        ]);

        $seeds = $seedRepository->findAll();

        $users = $userRepository->findAll();

        return $this->render('back/market/index.html.twig', [
            'markets' => array_diff($markets, $soldMarkets),
            'soldMarkets' => $soldMarkets,
            'selectedSeed' => $seed,
            'selectedUser' => $user,
            'seeds' => $seeds,
            'users' => $users,
            'backoffice' => true
        ]);
    }

    /**
     * @Route("/{id}/edit", name="market_edit", methods={"GET","POST"})
     * @IsGranted(MarketVoter::EDIT, subject="market")
     * @param Request $request
     * @param Market $market
     * @return Response
     */
    public function edit(Request $request, Market $market): Response
    {
        $seed = $market->getSeed();
        $market->setSeed(null);
        $market->setOwner(null);
        $form = $this->createForm(MarketType::class, $market,[
            'seed' => $seed,
            'owner' => $this->getUser()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'L\'offre a bien été modifié!');

            return $this->redirectToRoute('admin_market_index');
        }

        return $this->render('back/market/edit.html.twig', [
            'market' => $market,
            'form' => $form->createView(),
            'seed' => $seed,
            'backoffice' => true
        ]);
    }

    /**
     * @Route("/{id}/close", name="market_close", methods={"GET","POST"})
     * @IsGranted(MarketVoter::CLOSE, subject="market")
     * @param Request $request
     * @param Market $market
     * @return Response
     */
    public function close(Request $request, Market $market): Response
    {
        $market->setQuantity(0);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('market_index');
    }

    /**
     * @Route("/{id}", name="market_delete", methods={"DELETE"})
     * @IsGranted(MarketVoter::DELETE, subject="market")
     * @param Request $request
     * @param Market $market
     * @return Response
     */
    public function delete(Request $request, Market $market): Response
    {

        if ($this->isCsrfTokenValid('delete'.$market->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($market);
            $entityManager->flush();
        }

        return $this->redirectToRoute('market_index');
    }

    /**
     * @Route("/search", name="seed_search", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function search(Request $request, MarketRepository $marketRepository)
    {
        $body = json_decode($request->getContent());

        if (property_exists($body, 'user'))
            $user = $body->user;
        else
            $user = null;

        if (property_exists($body, 'seed'))
            $seed = $body->seed;
        else
            $seed = null;

        $data = $marketRepository->findBySeedAndOwner($seed, $user, true);
        return new Response(json_encode($data), Response::HTTP_OK);
    }
}
