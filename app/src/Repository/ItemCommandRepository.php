<?php

namespace App\Repository;

use App\Entity\ItemCommand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemCommand|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemCommand|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemCommand[]    findAll()
 * @method ItemCommand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemCommandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemCommand::class);
    }

    // /**
    //  * @return ItemCommand[] Returns an array of Command objects
    //  */

    public function findByMarketAndOwnerId($marketId,$ownerId)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.market = :market_id')
            ->andWhere('c.owner = :owner_id')
            ->setParameter('market_id', $marketId)
            ->setParameter('owner_id', $ownerId)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByMarketAndStatusPending($marketId) {
        return $this->createQueryBuilder('ic')
            ->join('ic.command', 'c')
            ->where('ic.market = :market_id')
            ->andWhere("c.status = 'pending'")
            ->setParameter('market_id', $marketId)
            ->getQuery()
            ->getResult()
        ;
    }
}
