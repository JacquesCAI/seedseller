<?php

namespace App\Form;

use App\Custom\Validator\Constraints\CheckAddress;
use App\Entity\Addresses;
use App\Entity\Command;
use App\Entity\User;
use App\Repository\AddressesRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandType extends AbstractType
{
    private $addressesRepository;

    public function __construct(AddressesRepository $addressesRepository) {
        $this->addressesRepository = $addressesRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', EntityType::class, [
                "class" => Addresses::class,
                "choices" => $this->addressesRepository->findBy([
                    'customer' => $options['owner']
                ]),
                "choice_label" => function (Addresses $address) {
                    return $address->getAddress() . ", " . $address->getZipcode() . " " . $address->getCity();
                },
                'constraints' => array(
                    new CheckAddress([
                        'message' => "Veuillez créer une adresse."
                    ])
                ),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
            'owner' => User::class
        ]);
    }
}
