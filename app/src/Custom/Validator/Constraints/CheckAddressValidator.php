<?php


namespace App\Custom\Validator\Constraints;

use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

class CheckAddressValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint) {
        if ($value == null) {
            $this->context->buildViolation($constraint->message)
                ->atPath('deliveryAddress')
                ->addViolation();
        }
    }
}