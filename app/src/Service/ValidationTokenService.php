<?php
namespace App\Service;

class ValidationTokenService {

    public static function generateToken($l = 15)
    {
        $token = '';
        $list = 'azertyuiopqsfdghjklmwxcvbnAZERTYUIOPQSFDGHJKLMWXCVBN1234567890!%$';
        for ($i = 0; $i<$l; $i++)
        {
            $token .= $list[rand(0,strlen($list) -1 )];
        }
        return $token;
    }
}