import './styles/global.scss';
import './styles/tailwind.css';

const $ = require('jquery');
require('bootstrap');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});

[...document.getElementsByClassName('flashMessage')]
    .forEach((message) => {
        message.style.cursor = 'pointer';
        message.addEventListener('click', () => message.style.display = 'none');
        window.setInterval(() => message.style.display = 'none', 15000);
    })
;
