var rangeElt = document.querySelector('input[type="range"]');
var priceElt = document.querySelector('.price');

var rangeValue = function(){
    var newValue = rangeElt.value;
    var newPrice = priceElt.getAttribute('data-price') * newValue;
    var target = document.querySelector('.value');
    target.innerHTML = newValue;
    priceElt.innerHTML = newPrice.toFixed(2);
}

rangeElt.addEventListener("input", rangeValue);