<?php

namespace App\Form;

use App\Custom\Validator\Constraints\ComplexityPassword;
use App\Custom\Validator\Constraints\ConfirmPassword;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez rentrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit faire minimum {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                    new ComplexityPassword([
                        'message' => "Votre mot de passe n'est pas assez complexe (doit faire au moins 6 caracères, avec une majuscule, une minuscule, un chiffre et un caractère spécial)"
                    ])
                ],
            ])
            ->add('confirm_password', PasswordType::class, array(
                'mapped' => false,
                'constraints' => array(
                    new NotBlank(),
                    new ConfirmPassword(['message' => 'Les deux mots de passe rentrés ne correspondent pas']),
                ),
            ));
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
