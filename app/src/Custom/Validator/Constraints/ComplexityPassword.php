<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

class ComplexityPassword extends Constraint
{
    public $message = 'Your passwords is not enough complex';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }

//    public function getTargets() {
//        return self::CLASS_CONSTRAINT;
//    }
}