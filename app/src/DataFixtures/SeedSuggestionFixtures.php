<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\SeedSuggestion;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SeedSuggestionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        $faker->addProvider(new \FakerRestaurant\Provider\fr_FR\Restaurant($faker));

        $categories = $manager->getRepository(Category::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();
        $seedsName = ["Menthe", "Fraise", "Œillet d'Inde"];

        $images = ["seed_picture1.jpeg", "seed_picture2.jpeg", "seed_picture3.jpeg", "seed_picture4.jpeg",
            "seed_picture5.jpeg", "seed_picture6.jpeg"];

        for ($i = 0; $i < count($seedsName); $i++) {

            $suggestor = $users[array_rand($users)];

            $seed = (new SeedSuggestion())
                ->setSeedName($seedsName[$i])
                ->setImage($images[array_rand($images)])
                ->setCategoryName($categories[array_rand($categories)])
                ->setMeasurementUnit('kg')
                ->setDescription($faker->text)
                ->setAdditionalInformation($faker->text)
                ->setSuggestorFirstname($suggestor->getFirstname())
                ->setSuggestionLastname($suggestor->getLastname())
                ->setSuggestorEmail($suggestor->getEmail())
                ->setValidated(false)
                ->setLifting($faker->text)
                ->setNumberSeedsPerGram($faker->numberBetween(1, 1000))
                ->setPlantingDensity($faker->numberBetween(1, 100))
                ->setSowingdate($faker->monthName)
                ->setHarvestdate($faker->monthName)
                ->setCultivationPractice($faker->text)
                ->setDiseasesandMainPests($faker->text)
                ->setAdditionalInformation($faker->text)
                ;
            ;
            $manager->persist($seed);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class
        ];
    }
}
