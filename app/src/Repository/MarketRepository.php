<?php

namespace App\Repository;

use App\Entity\Market;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Market|null find($id, $lockMode = null, $lockVersion = null)
 * @method Market|null findOneBy(array $criteria, array $orderBy = null)
 * @method Market[]    findAll()
 * @method Market[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Market::class);
    }

    /**
      * @return Market[] Returns an array of Market objects
      */
    public function findBySeed($seed_id)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.seed = :seed_id')
            ->setParameter('seed_id', $seed_id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findBySeedAndQuantityOver0($seed_id, array $boughtMarketIds)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.seed = :seed_id')
            ->andWhere(count($boughtMarketIds) > 0 ? 'm.id not in ('.implode(', ',$boughtMarketIds).')' : "1 = 1")
            ->andWhere('m.quantity > 0')
            ->setParameter('seed_id', $seed_id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findOver0ByOwner($owner_id) {
        return $this->createQueryBuilder('m')
            ->where("m.owner = :owner_id")
            ->andWhere("m.quantity > 0")
            ->setParameter("owner_id", $owner_id)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findBySeedAndOwner(int $seed = null,int $owner = null, $json = false) {
        $query = $this->createQueryBuilder('m');

        if ($json) {
            $query->select('m.id, o.firstname, o.lastname, o.image, m.quantity, s.measurementUnit, m.price, m.canSeparate, s.name as seedName, s.id as seedId');
        }

        $query
            ->innerJoin('m.owner', 'o')
            ->innerJoin('m.seed', 's');
        if ($seed != null) {
            $query->where("m.seed = :seed_id")
                ->setParameter('seed_id', $seed);
        }
        if ($owner != null) {
            if ($seed != null) {
                $query->andWhere('m.owner = :owner_id');
            } else {
                $query->where('m.owner = :owner_id');
            }
            $query->setParameter('owner_id', $owner);
        }
        return  $query->orderBy('m.id','ASC')->getQuery()->getResult();
    }

    public function findActiveMarkets($owner_id) {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.itemCommands', 'ic')
            ->leftJoin('ic.command', 'c')
            ->where("m.owner = :owner_id")
            ->andWhere("m.quantity > 0 or c.status = 'pending'")
            ->setParameter("owner_id", $owner_id)
            ->getQuery()
            ->getResult()
        ;
    }
}
