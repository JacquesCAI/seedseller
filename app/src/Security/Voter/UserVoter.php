<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

use App\Entity\User;

class UserVoter extends Voter {

    const EDIT = 'edit';
    const DELETE = 'delete';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT, self::DELETE])) {
            return false;
        }

        // only vote on `User` objects
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        if (!$currentUser instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a User object, thanks to `supports()`
        /** @var User $user */
        $user = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($user, $currentUser);
            case self::DELETE:
                return $this->canDelete($user, $currentUser);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canEdit(User $user, User $currentUser)
    {
        return $user->getId() === $currentUser->getId() || $this->security->isGranted('ROLE_ADMIN');
    }

    private function canDelete(User $user, User $currentUser)
    {
        return $this->canEdit($user, $currentUser);
    }
}