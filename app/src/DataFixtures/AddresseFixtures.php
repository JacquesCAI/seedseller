<?php

namespace App\DataFixtures;

use App\Entity\Addresses;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AddresseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < count($users); $i++) {
            $addresse = (new Addresses())
                ->setCity($faker->city)
                ->setZipcode(intval($faker->postcode))
                ->setAddress($faker->streetAddress)
                ->setCustomer($users[$i]);

            $manager->persist($addresse);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
