<?php

namespace App\Repository;

use App\Entity\Seed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Seed|null find($id, $lockMode = null, $lockVersion = null)
 * @method Seed|null findOneBy(array $criteria, array $orderBy = null)
 * @method Seed[]    findAll()
 * @method Seed[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Seed::class);
    }

    public function findTop5CardsByOfferCount()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            select s.id, s.name, s.image, s.measurement_unit, nbOffer, avgPrice, c.name as category
            from seed s
            left join (select count(m.id) as nbOffer, avg(m.price) as avgPrice, m.seed_id from market m where m.quantity > 0 group by m.seed_id) m
            on s.id = m.seed_id
            join category c on c.id = s.category_id
            order by m.nbOffer DESC
            limit 5
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAllAssociative();
    }

    public function findCardsByCategoryAndSearch(string $search, int $category = null) {

        $query = $this->createQueryBuilder('s')
            ->select('s.id, s.name, s.measurementUnit, c.name as category, count(m) as nbOffers, avg(m.price) as avgPrice, s.image')
            ->join('s.category', 'c')
            ->leftJoin('s.markets', 'm', 'WITH', 'm.quantity > 0')
            ->where("UNACCENT(LOWER(s.name)) LIKE UNACCENT(LOWER(:search))")
            ->orWhere("UNACCENT(LOWER(c.name)) LIKE UNACCENT(LOWER(:search))")
        ;

        if($category != null) {
            $query
                ->andWhere("c.id = :category")
                ->setParameter('category', $category)
            ;
        }

        $query
            ->setParameter('search', "%$search%")
            ->groupBy('s.id, c.id')
            ->orderBy('s.id', 'ASC')
        ;

        return  $query->getQuery()->getResult();
    }
}
