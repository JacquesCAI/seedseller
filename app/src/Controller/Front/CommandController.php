<?php

namespace App\Controller\Front;

use App\Entity\ItemCommand;
use App\Entity\Command;
use App\Repository\CommandRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Security\Voter\CommandVoter;
use App\Security\Voter\CartVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/command")
 */
class CommandController extends AbstractController
{
    /**
     * @Route("/", name="command_index", methods={"GET"})
     */
    public function index(CommandRepository $commandRepository): Response
    {
        return $this->render('front/command/index.html.twig', [
            'commands' => $commandRepository->findCommands($this->getUser()->getId())
        ]);
    }

    /**
     * @Route("/{id}", name="command_show")
     * @IsGranted(CommandVoter::VIEW, subject="command")
     */
    public function show(Command $command): Response
    {
        return $this->render('front/command/show.html.twig', [
            'command' => $command,
        ]);
    }

    /**
     * @Route("/item/{id}", name="command_show_item", methods={"GET"})
     * @IsGranted(CartVoter::VIEW, subject="itemCommand")
     */
    public function show_item(ItemCommand $itemCommand): Response
    {
        return $this->render('front/command/show_item.html.twig', [
            'itemCommand' => $itemCommand,
            'cart' => $itemCommand->getCommand()->getStatus() == Command::PENDING_STATUS
        ]);
    }
}
