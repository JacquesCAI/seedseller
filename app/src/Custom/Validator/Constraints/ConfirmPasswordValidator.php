<?php
namespace App\Custom\Validator\Constraints;

use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

//use Doctrine\ORM\EntityManager;

class ConfirmPasswordValidator extends ConstraintValidator {

//    protected $em;
//
//    public function __construct(EntityManager $entityManager) {
//        $this->em = $entityManager;
//    }

    public function validate($value, Constraint $constraint) {
        $password = $this->context->getRoot()->get('password')->getData();
        if ($password != $value) {
            $this->context->buildViolation($constraint->message)
                ->atPath('password')
                ->addViolation();
        }
    }

}