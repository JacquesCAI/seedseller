<?php

namespace App\Security\Voter;

use App\Entity\Command;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;

class CommandVoter extends Voter {

    const VIEW = 'view';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::VIEW])) {
            return false;
        }

        // only vote on `Command` objects
        if (!$subject instanceof Command) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Command object, thanks to `supports()`
        /** @var Command $command */
        $command = $subject;
        switch ($attribute) {
            case self::VIEW:
                return $this->canView($command, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Command $command, User $user)
    {
        return $user->getId() === $command->getOwner()->getId();
    }
}