<?php

namespace App\Controller\Back;

use App\Repository\CommandRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/command")
 */
class CommandController extends AbstractController
{
    /**
     * @Route("/list/{owner}/{deliveryMan}", name="command_index")
     */
    public function index(int $owner = null, int $deliveryMan = null, CommandRepository $commandRepository, UserRepository $userRepository): Response
    {
        $owner = $owner == 0 ? null : $owner;
        $deliveryMan = $deliveryMan == 0 ? null : $deliveryMan;
        if ($owner != null) {
            $owner = $owner != 0 ? $userRepository->find($owner) : null;
        }
        if ($deliveryMan != null) {
            $deliveryMan = $deliveryMan != 0 ? $userRepository->find($deliveryMan) : null;
        }

        $commands = $commandRepository->findNotPendingCommandsBydeliveryManAndOwner(false,$deliveryMan != null ? $deliveryMan->getId() : "all",$owner != null ? $owner->getId() : "all");
        $owners = $userRepository->findAll();
        $deliveryMans = array_filter($owners, function ($owner) {
           return in_array('ROLE_DELIVERY', $owner->getRoles());
        });

        return $this->render('back/command/index.html.twig', [
            'commands' => $commands,
            'deliveryMans' => $deliveryMans,
            'owners' => $owners,
            'selectedOwner' => $owner,
            'selectedDeliveryMan' => $deliveryMan
        ]);
    }

    /**
     * @Route("/search", name="command_search", methods={"POST"})
     */
    public function search(Request $request, CommandRepository $commandRepository) {
        $body = json_decode($request->getContent());

        $data = $commandRepository->findNotPendingCommandsBydeliveryManAndOwner(true, $body->deliveryMan, $body->owner);
        return new Response(json_encode($data), Response::HTTP_OK);
    }
}
