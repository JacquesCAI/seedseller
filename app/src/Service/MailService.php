<?php
namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailService {

    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendTemplateMail($targetMail, $subject, $template, $params = [])
    {
        $email = (new TemplatedEmail())
            ->from('seedseller@noreply.fr')
            ->to($targetMail)
            ->subject($subject)
            ->htmlTemplate('emails/'. $template)
            ->context($params)
        ;

            $this->mailer->send($email);
    }

}