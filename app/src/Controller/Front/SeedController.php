<?php

namespace App\Controller\Front;

use App\Entity\Seed;
use App\Form\SeedType;
use App\Repository\CategoryRepository;
use App\Repository\CommandRepository;
use App\Repository\MarketRepository;
use App\Repository\SeedRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Security\Voter\SeedVoter;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/seed")
 */
class SeedController extends AbstractController
{
    /**
     * @Route("/", name="seed_index", methods={"GET"})
     * @param SeedRepository $seedRepository
     * @return Response
     */
    public function index(SeedRepository $seedRepository, CategoryRepository $categoryRepository): Response
    {
        return $this->render('front/seed/index.html.twig', [
            'seeds' => $seedRepository->findAll(),
            'categories' => $categoryRepository->findAll(),
            'backoffice' => false
        ]);
    }

    /**
     * @Route("/new", name="seed_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $seed = new Seed();
        $form = $this->createForm(SeedType::class, $seed);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seed);
            $entityManager->flush();

            return $this->redirectToRoute('seed_index');
        }

        return $this->render('front/seed/new.html.twig', [
            'seed' => $seed,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/more-information", name="seed_more_information", methods={"GET"})
     * @param Seed $seed
     * @return Response
     */
    public function moreInformation(Seed $seed): Response
    {
        return $this->render('front/seed/more_information.html.twig', [
            'seed' => $seed,
            'backoffice' => false
        ]);
    }

    /**
     * @Route("/{id}", name="seed_show", methods={"GET"})
     * @param Seed $seed
     * @return Response
     */
    public function show(Seed $seed, MarketRepository $marketRepository, CommandRepository $commandRepository): Response
    {
        $boughtMarketIds = [];
        $user = $this->getUser();
        if ($user) {
            $cart = $commandRepository->findPendingCommandByOwnerId($this->getUser()->getId());

            if ($cart != null) {
                foreach ($cart->getCommandItems() as $item) {
                    $boughtMarketIds[] = $item->getMarket()->getId();
                }
            }
        }
        $markets = $marketRepository->findBySeedAndQuantityOver0($seed->getId(), $boughtMarketIds);

        $sumPrice = 0;
        foreach ($markets as $market)
            $sumPrice += $market->getPrice();

        $averagePrice = count($markets) > 0 ? round($sumPrice/count($markets), 2) : 0;

        return $this->render('front/seed/show.html.twig', [
            'seed' => $seed,
            'markets' => $markets,
            'averagePrice' => $averagePrice,
            'backoffice' => false
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seed_edit", methods={"GET","POST"})
     * @IsGranted(subject="seed")
     * @param Request $request
     * @param Seed $seed
     * @IsGranted(SeedVoter::EDIT, subject="seed")
     * @return Response
     */
    public function edit(Request $request, Seed $seed): Response
    {
        $form = $this->createForm(SeedType::class, $seed);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_seed_index');
        }

        return $this->render('front/seed/edit.html.twig', [
            'seed' => $seed,
            'form' => $form->createView(),
            'backoffice' => false
        ]);
    }

    /**
     * @Route("/{id}", name="seed_delete", methods={"DELETE"})
     * @IsGranted(subject="seed")
     * @param Request $request
     * @param Seed $seed
     * @return Response
     */
    public function delete(Request $request, Seed $seed): Response
    {
        if ($this->isCsrfTokenValid('delete'.$seed->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($seed);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_seed_index');
    }

    /**
     * @Route("/search", name="seed_search", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function search(Request $request, SeedRepository $seedRepository)
    {
        $body = json_decode($request->getContent());

        if (property_exists($body, 'category'))
            $category = $body->category;
        else
            $category = null;

        $data = $seedRepository->findCardsByCategoryAndSearch($body->search, $category);
        return new Response(json_encode($data), Response::HTTP_OK);
    }
}
