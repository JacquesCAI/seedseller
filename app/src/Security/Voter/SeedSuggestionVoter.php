<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

use App\Entity\SeedSuggestion;
use App\Entity\User;

class SeedSuggestionVoter extends Voter {

    const EDIT = 'edit';
    const DELETE = 'delete';
    const VALIDATE = 'validate';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT, self::DELETE, self::VALIDATE])) {
            return false;
        }
        // only vote on `SeedSuggestion` objects
        if (!$subject instanceof SeedSuggestion) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit();
            case self::DELETE:
                return $this->canDelete();
            case self::VALIDATE:
                return $this->canValidate();
        }

        throw new \LogicException('This code should not be reached!');
    }

    protected function canEdit() {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    protected function canDelete() {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    protected function canValidate() {
        return $this->security->isGranted('ROLE_ADMIN');
    }


}