<?php

namespace App\Controller\Front;

use App\Entity\Market;
use App\Form\MarketType;
use App\Repository\ItemCommandRepository;
use App\Repository\MarketRepository;
use App\Repository\SeedRepository;
use App\Repository\CategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Security\Voter\MarketVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/market")
 */
class MarketController extends AbstractController
{
    /**
     * @Route("/", name="market_index", methods={"GET"})
     * @param MarketRepository $marketRepository
     * @return Response
     */
    public function index(MarketRepository $marketRepository): Response
    {
        $activeMarkets = $marketRepository->findActiveMarkets($this->getUser()->getId());

        $allMarkets = $marketRepository->findBy([
            'owner' => $this->getUser(),
        ]);


        return $this->render('front/market/index.html.twig', [
            'markets' => $activeMarkets,
            'soldMarkets' => array_diff($allMarkets, $activeMarkets)
        ]);
    }

    /**
     * @Route("/{id}/new", name="market_new", methods={"GET","POST"})
     */
    public function new(int $id, Request $request, SeedRepository $seedRepository): Response
    {
        $market = new Market();
        $form = $this->createForm(MarketType::class, $market, [
            'seed' => $seedRepository->find($id),
            'owner' => $this->getUser()
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $market->setOwner($this->getUser());
            $market->setInitialQuantity($market->getQuantity());
            $entityManager->persist($market);
            $entityManager->flush();

            $this->addFlash('success', 'Votre vente a bien été ajouté à la liste des offres');

            return $this->redirectToRoute('seed_show', ['id' => $id]);
        }

        return $this->render('front/market/new.html.twig', [
            'market' => $market,
            'seed' => $seedRepository->find($id),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="market_show", methods={"GET"})
     * @IsGranted(MarketVoter::VIEW, subject="market")
     * @param Market $market
     * @return Response
     */
    public function show(Market $market, MarketRepository $marketRepository, SeedRepository $seedRepository): Response
    {
        return $this->render('front/market/show.html.twig', [
            'market' => $market
        ]);
    }

    /**
     * @Route("/{id}/edit", name="market_edit", methods={"GET","POST"})
     * @IsGranted(MarketVoter::EDIT, subject="market")
     * @param Request $request
     * @param Market $market
     * @return Response
     */
    public function edit(Request $request, Market $market, ItemCommandRepository $itemCommandRepository): Response
    {
        $seed = $market->getSeed();
        $market->setSeed(null);
        $market->setOwner(null);
        $form = $this->createForm(MarketType::class, $market, [
            'seed' => $seed,
            'owner' => $this->getUser()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $itemsInCart = $itemCommandRepository->findByMarketAndStatusPending($market->getId());
            foreach ($itemsInCart as $itemInCart) {
                $em->remove($itemInCart);
            }
            $em->flush();
            $this->addFlash('success', 'L\'offre a bien été modifié!');

            return $this->redirectToRoute('market_show', ['id' => $market->getId()]);
        }

        return $this->render('front/market/edit.html.twig', [
            'market' => $market,
            'seed' => $seed,
            'form' => $form->createView(),
            'backoffice' => false,
            'referer' => $request->headers->get('referer')
        ]);
    }

    /**
     * @Route("/{id}/close", name="market_close", methods={"GET","POST"})
     * @IsGranted(MarketVoter::CLOSE, subject="market")
     * @param Request $request
     * @param Market $market
     * @return Response
     */
    public function close(Request $request, Market $market, ItemCommandRepository $itemCommandRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $itemsInCart = $itemCommandRepository->findByMarketAndStatusPending($market->getId());
        foreach ($itemsInCart as $itemInCart) {
            $em->remove($itemInCart);
        }
        $market->setQuantity(0);
        $em->flush();
        $this->addFlash('success', 'Votre offre a bien été fermé. Vous devez néanmoins fournir les 
            quantitées commandées.');
        return $this->redirectToRoute('market_index');
    }

    /**
     * @Route("/{id}", name="market_delete", methods={"DELETE"})
     * @IsGranted(MarketVoter::DELETE, subject="market")
     * @param Request $request
     * @param Market $market
     * @return Response
     */
    public function delete(Request $request, Market $market): Response
    {

        if ($this->isCsrfTokenValid('delete'.$market->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($market);
            $entityManager->flush();
            $this->addFlash('success', 'Votre offre a bien été supprimé.');
        }

        return $this->redirectToRoute('market_index');
    }
}
