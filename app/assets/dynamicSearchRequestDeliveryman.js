
let searchStr = "";

const fetchData = () => {

    const data = {
        search: searchStr
    };

    fetch("/admin/user/search_request_delivery", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        },
        body: JSON.stringify(data)
    })
        .then(response => {console.log(response.json()); return response.json()})
        .then(data => displayCards(data));

};
const buildCard = (data) => {
    const prototype = document.getElementById("request-delivery-prototype");

    const newCard = prototype.cloneNode(true);
    newCard.removeAttribute('id');
    newCard.classList.remove(['d-none']);
    newCard.classList.add(["d-lg-flex"])

    const ownerPicture = newCard.querySelector(".owner-picture");
    ownerPicture.src = "/uploads/images/profile_pic/" + (data.image == null ? "default_profile_pic.png" : data.image);

    const name = newCard.querySelector(".name");
    name.classList.remove(["name"]);
    name.innerText = data.firstname+" "+data.lastname;

    const email = newCard.querySelector(".email");
    email.classList.remove(["email"]);
    email.innerText = data.email;

    const nbCommands = newCard.querySelector(".nbCommands");
    nbCommands.classList.remove(["nbCommands"]);
    nbCommands.innerText = data.nbCommands+" commandes réalisées";

    const nbMarketLink = newCard.querySelector(".nbMarketLink");
    nbMarketLink.classList.remove(["nbMarketLink"]);
    nbMarketLink.href = "/admin/market/list/0/"+data.id;
    nbMarketLink.querySelector("strong").innerText = data.nbMarkets+" offres créées";

    return newCard;
};

const displayCards = (datas) => {
    const container = document.getElementById("deliveryman");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    if (datas.length > 0) {
        datas.forEach(data => {
            const card = buildCard(data);
            container.appendChild(card);
        });
    } else {
        const nothingSeed = document.createElement("span");
        nothingSeed.appendChild(document.createTextNode("Aucun utilisateur ne correspond à votre recherche"));
        container.appendChild(nothingSeed);
    }
};

const searchField = document.getElementById('search-request-delivery');

searchField.addEventListener('input', ({target}) => {
    searchStr = target.value;
    fetchData();
});