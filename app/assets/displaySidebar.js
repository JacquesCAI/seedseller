const openNav = () => {
    document.getElementById("sidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("sidebar-arrow").style.transform = 'rotate(180deg)';
    document.getElementById("sidebar-text").innerHTML = 'Masquer les catégories';
}

const closeNav = () => {
    document.getElementById("sidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("sidebar-arrow").style.transform = 'rotate(0deg)';
    document.getElementById("sidebar-text").innerHTML = 'Voir les catégories';
}

const toogle = () => {
    const sidebarWidth = document.getElementById("sidebar").style.width;
    if (!sidebarWidth || sidebarWidth == "0px") {
        openNav();
    } else {
        closeNav();
    }
}

const sidebarArrow = document.getElementById("sidebar-btn");
sidebarArrow.addEventListener('click', toogle);