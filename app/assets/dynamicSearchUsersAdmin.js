
let searchStr = "";
let searchRole = "ROLE_USER";

const fetchData = () => {

    const data = {
        search: searchStr,
        role: searchRole
    };

    fetch("/admin/user/search", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(data => displayCards(data));

};
const buildCard = (data) => {
    const prototype = document.getElementById("user-prototype");

    const newCard = prototype.cloneNode(true);
    newCard.removeAttribute('id');
    newCard.classList.remove(['d-none']);
    newCard.classList.add(["d-lg-flex"])

    const ownerPicture = newCard.querySelector(".owner-picture");
    ownerPicture.src = "/uploads/images/profile_pic/" + (data.image == null ? "default_profile_pic.png" : data.image);

    const name = newCard.querySelector(".name");
    if(!data.roles.includes("ROLE_DELIVERY")){
        name.querySelector('i').remove();
    }
    name.classList.remove(["name"]);
    name.querySelector('span').innerText = data.firstname+" "+data.lastname;

    const email = newCard.querySelector(".email");
    email.classList.remove(["email"]);
    email.innerText = data.email;

    const nbCommandLink = newCard.querySelector(".nbCommandLink");
    nbCommandLink.classList.remove(["nbCommandLink"]);
    nbCommandLink.href = "/admin/command/list/"+data.id;
    nbCommandLink.querySelector("strong").innerText = data.nbCommands+" commandes réalisées";

    const nbCommandsToDeliveryDiv = newCard.querySelector(".nbCommandsToDeliveryDiv");
    if (data.roles.includes("ROLE_DELIVERY")) {
        nbCommandsToDeliveryDiv.classList.remove(["nbCommandsToDeliveryDiv"]);
        nbCommandsToDeliveryDiv.querySelector("a").href = "/admin/command/list/0/"+data.id;
        nbCommandsToDeliveryDiv.querySelector("strong").innerText = data.nbCommandsToDelivery + " livraison"+(data.nbCommandsToDelivery > 1 ? 's' : '');
    } else {
        nbCommandsToDeliveryDiv.remove();
    }

    const nbMarketLink = newCard.querySelector(".nbMarketLink");
    nbMarketLink.classList.remove(["nbMarketLink"]);
    nbMarketLink.href = "/admin/market/list/0/"+data.id;
    nbMarketLink.querySelector("strong").innerText = data.nbMarkets+" offres créées";

    const userShowLink = newCard.querySelector(".showLink");
    userShowLink.classList.remove(["showLink"]);
    userShowLink.href = "/admin/user/show/"+data.id;

    return newCard;
};

const displayCards = (datas) => {
    const container = document.getElementById("users");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    if (datas.length > 0) {
        datas.forEach(data => {
            const card = buildCard(data);
            container.appendChild(card);
        });
    } else {
        const nothingSeed = document.createElement("span");
        nothingSeed.appendChild(document.createTextNode("Aucun utilisateur ne correspond à votre recherche"));
        container.appendChild(nothingSeed);
    }
};

const searchField = document.getElementById('search');

searchField.addEventListener('input', ({target}) => {
    searchStr = target.value;
    fetchData();
});

const searchRoleField = document.getElementById('role');
searchRoleField.addEventListener('change', ({target}) => {
    searchRole = target.value;
    fetchData();
});