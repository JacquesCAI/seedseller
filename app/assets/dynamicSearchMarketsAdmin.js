
let seedId = null,
    userId = null;

const fetchData = () => {

    const data = {
        seed: seedId,
        user: userId
    };

    fetch("/admin/market/search", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(data => displayCards(data));

};
const buildCard = (data) => {
    const prototype = document.getElementById("card-prototype");

    const newCard = prototype.cloneNode(true);
    newCard.removeAttribute('id');
    newCard.classList.remove(['d-none']);
    newCard.classList.add(["d-lg-flex"]);

    const name = newCard.querySelector(".name");
    name.classList.remove(["name"]);
    name.innerText = data.firstname+" "+data.lastname;

    const units = newCard.querySelector(".units");
    units.classList.remove(["units"]);
    units.innerText = '\u00A0'+data.quantity + " " + data.measurementUnit;

    const priceByUnit = newCard.querySelector(".priceByUnit");
    priceByUnit.classList.remove(["priceByUnit"]);
    priceByUnit.innerText = '\u00A0'+data.price+" €/"+data.measurementUnit;

    const canSeparate = newCard.querySelector(".canSeperate");
    canSeparate.classList.remove(["canSeparate"]);
    if (!data.canSeparate) {
        canSeparate.remove();
    }

    const editLink = newCard.querySelector(".editLink");
    editLink.classList.remove(["editLink"]);
    editLink.href = "/admin/market/"+data.id+"/edit";

    const ownerPicture = newCard.querySelector(".owner-picture");
    ownerPicture.src =  "/uploads/images/profile_pic/"+ (data.image == null ? "default_profile_pic.png" : data.image);

    const seedLink = newCard.querySelector(".seedLink");
    seedLink.classList.remove(["seedLink"]);
    seedLink.innerText = data.seedName;
    seedLink.href = "/admin/seed/"+data.seedId+"/more-information";

    return newCard;
};

const displayCards = (datas) => {
    document.getElementById("number_offer").querySelector("strong").innerText = datas.length;
    const container = document.getElementById("markets");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    if (datas.length > 0) {
        datas.forEach(data => {
            const card = buildCard(data);
            container.appendChild(card);
        });
    } else {
        const nothingSeed = document.createElement("span");
        nothingSeed.appendChild(document.createTextNode("Aucun market ne correspond à votre recherche"));
        container.appendChild(nothingSeed);
    }
};

const searchFieldUser = document.getElementById('select_user');
const searchFieldSeed = document.getElementById('select_seed')

searchFieldUser.addEventListener('change', ({target}) => {
    userId = target.value === '0' ? null : target.value;
    fetchData();
});

searchFieldSeed.addEventListener('change', ({target}) => {
    seedId = target.value === '0' ? null : target.value;
    fetchData();
});