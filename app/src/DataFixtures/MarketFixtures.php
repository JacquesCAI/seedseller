<?php

namespace App\DataFixtures;

use App\Entity\Seed;
use App\Entity\User;
use App\Entity\Market;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MarketFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $seeds = $manager->getRepository(Seed::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 200; $i++) {

            $seed = $seeds[array_rand($seeds)];
            $quantity = $faker->randomFloat($nbMaxDecimals = 2, $min = 0.01, $max = 20);
            $market = (new Market())
                ->setOwner($users[array_rand($users)])
                ->setSeed($seed)
                ->setQuantity($quantity)
                ->setPrice($faker->randomFloat($nbMaxDecimals = 2, $min = 0.50, $max = 6))
                ->setInitialQuantity($quantity)
            ;

            if(rand(0,1)) {
                $market
                    ->setCanSeparate(1)
                    ->setMinimumQuantity($faker->numberBetween($min = 1, $max = $quantity));
            } else {
                $market
                    ->setCanSeparate(0);
            }
            $manager->persist($market);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SeedFixtures::class,
            UserFixtures::class
        ];
    }
}
