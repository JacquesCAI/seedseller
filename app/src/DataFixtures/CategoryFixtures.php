<?php

namespace App\DataFixtures;

use App\Entity\Category;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $categoriesName = [ "Graines de bébé", "Graines créoles", "Graines comestibles",
            "Graines de fleurs", "Graines de fruits", "Graines de légumes",
            "Graines hybrides", "Graines améliorées"];

        for ($i = 0; $i < count($categoriesName); $i++) {
            $category = (new Category())
                ->setName($categoriesName[$i]);

            $manager->persist($category);
        }

        $manager->flush();
    }
}
