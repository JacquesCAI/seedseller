<?php

namespace App\Entity;

use App\Repository\SeedSuggestionRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints\Positive;

/**
 * @ORM\Entity(repositoryClass=SeedSuggestionRepository::class)
 * @Vich\Uploadable
 */
class SeedSuggestion
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="seed_pic", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoryName;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $measurementUnit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $suggestorFirstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $suggestionLastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $suggestorEmail;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $additionalInformation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $validated;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $seedName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $lifting;

    /**
     * @Positive(
     *      message="Le nombre de graines / gr doit être supérieure à 0"
     * )
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $numberSeedsPerGram;

    /**
     * @Positive(
     *      message="La densité de plantation doit être supérieure à 0"
     * )
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $plantingDensity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $cultivationPractice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $diseasesAndMainPests;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sowingdate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $harvestdate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getCategoryName(): ?Category
    {
        return $this->categoryName;
    }

    public function setCategoryName(?Category $categoryName): self
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    public function getMeasurementUnit(): ?string
    {
        return $this->measurementUnit;
    }

    public function setMeasurementUnit(string $measurementUnit): self
    {
        $this->measurementUnit = $measurementUnit;

        return $this;
    }

    public function getSuggestorFirstname(): ?string
    {
        return $this->suggestorFirstname;
    }

    public function setSuggestorFirstname(string $suggestorFirstname): self
    {
        $this->suggestorFirstname = $suggestorFirstname;

        return $this;
    }

    public function getSuggestionLastname(): ?string
    {
        return $this->suggestionLastname;
    }

    public function setSuggestionLastname(string $suggestionLastname): self
    {
        $this->suggestionLastname = $suggestionLastname;

        return $this;
    }

    public function getSuggestorEmail(): ?string
    {
        return $this->suggestorEmail;
    }

    public function setSuggestorEmail(string $suggestorEmail): self
    {
        $this->suggestorEmail = $suggestorEmail;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

    public function getValidated(): ?bool
    {
        return $this->validated;
    }

    public function setValidated(bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    public function getSeedName(): ?string
    {
        return $this->seedName;
    }

    public function setSeedName(string $seedName): self
    {
        $this->seedName = $seedName;

        return $this;
    }

    public function getLifting(): ?string
    {
        return $this->lifting;
    }

    public function setLifting(?string $lifting): self
    {
        $this->lifting = $lifting;

        return $this;
    }

    public function getNumberSeedsPerGram(): ?int
    {
        return $this->numberSeedsPerGram;
    }

    public function setNumberSeedsPerGram(?int $numberSeedsPerGram): self
    {
        $this->numberSeedsPerGram = $numberSeedsPerGram;

        return $this;
    }

    public function getPlantingDensity(): ?int
    {
        return $this->plantingDensity;
    }

    public function setPlantingDensity(?int $plantingDensity): self
    {
        $this->plantingDensity = $plantingDensity;

        return $this;
    }

    public function getSowingdate(): ?string
    {
        return $this->sowingdate;
    }

    public function setSowingdate(?string $sowingdate): self
    {
        $this->sowingdate = $sowingdate;

        return $this;
    }

    public function getCultivationPractice(): ?string
    {
        return $this->cultivationPractice;
    }

    public function setCultivationPractice(?string $cultivationPractice): self
    {
        $this->cultivationPractice = $cultivationPractice;

        return $this;
    }

    public function getDiseasesAndMainPests(): ?string
    {
        return $this->diseasesAndMainPests;
    }

    public function setDiseasesAndMainPests(?string $diseasesAndMainPests): self
    {
        $this->diseasesAndMainPests = $diseasesAndMainPests;

        return $this;
    }

    public function getHarvestdate(): ?string
    {
        return $this->harvestdate;
    }

    public function setHarvestdate(?string $harvestdate): self
    {
        $this->harvestdate = $harvestdate;

        return $this;
    }
}
