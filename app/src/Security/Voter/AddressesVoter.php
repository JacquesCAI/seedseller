<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

use App\Entity\Addresses;
use App\Entity\User;

class AddressesVoter extends Voter {

    const EDIT = 'edit';
    const DELETE = 'delete';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT, self::DELETE])) {
            return false;
        }

        // only vote on `Addresses` objects
        if (!$subject instanceof Addresses) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Addresses object, thanks to `supports()`
        /** @var Addresses $addresses */
        $addresses = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($addresses, $user);
            case self::DELETE:
                return $this->canDelete($addresses, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canEdit(Addresses $addresses, User $user)
    {
        return $user === $addresses->getCustomer() || $this->security->isGranted('ROLE_ADMIN');
    }

    private function canDelete(Addresses $addresses, User $user)
    {
        return $this->canEdit($addresses, $user);
    }
}