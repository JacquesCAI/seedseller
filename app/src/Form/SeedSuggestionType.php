<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\SeedSuggestion;
use App\Service\FileUploader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SeedSuggestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seedName')
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                "image_uri" => true,
                "download_uri" => false,
                "allow_delete" => false,

                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => "La photo de la graine doit être des types suivants : .jpeg, .jpg ou .png"
                    ])
                ]
            ])
            ->add('categoryName', EntityType::class, [
                "class" => Category::class,
                "choice_label" => function ($category) {
                    return $category->getName();
                }
            ])
            ->add('measurementUnit', ChoiceType::class, [
                'choices' => [
                    'kg' => 'kg',
                    'g' => 'g',
                ]
            ])
            ->add('description')
            ->add('additionalInformation')
            ->add('lifting', null, ['label'=>false])
            ->add('numberSeedsPerGram')
            ->add('plantingDensity')
            ->add('sowingdate')
            ->add('harvestdate')
            ->add('cultivationPractice')
            ->add('diseasesAndMainPests')
            ->add('suggestorFirstname', HiddenType::class)
            ->add('suggestionLastname', HiddenType::class)
            ->add('suggestorEmail', HiddenType::class)
            ->add('validated', HiddenType::class, [
                'empty_data' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SeedSuggestion::class,
        ]);
    }
}
