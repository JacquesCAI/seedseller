
let deliveryMan = "all";
let owner = "all";

const fetchData = () => {

    const data = {
        deliveryMan,
        owner
    };

    fetch("/admin/command/search", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(data => displayCards(data));

};
const buildCard = (data) => {
    const prototype = document.getElementById("command-prototype");

    const newCard = prototype.cloneNode(true);
    newCard.removeAttribute('id');
    newCard.classList.remove(['d-none']);
    newCard.classList.add(["d-lg-flex"]);

    const ownerPicture = newCard.querySelector(".owner-picture");
    ownerPicture.src = "/uploads/images/profile_pic/" + (data.ownerImage == null ? "default_profile_pic.png" : data.ownerImage);

    const ownerNames = newCard.querySelector(".owner-names");
    ownerNames.classList.remove(["owner-names"]);
    ownerNames.querySelector('span').innerText = data.ownerFirstname+" "+data.ownerLastname;
    const totalPrice = newCard.querySelector(".totalPrice");
    totalPrice.classList.remove(["totalPrice"]);
    totalPrice.innerText = data.totalPrice ? (Math.round(parseFloat(data.totalPrice)*(10**2))/(10**2))+"€" : 0+"€";

    const address = newCard.querySelector(".address");
    address.classList.remove(["address"]);
    address.innerText = data.city+" "+data.address;

    const status = newCard.querySelector(".status");
    status.classList.remove(["status"]);
    if (data.status === "paid") {
        status.innerText = "Payée";
    } else if (data.status === "dispatched") {
        status.innerText = "En cours d'acheminement";
    } else if (data.status === "delivered") {
        status.innerText = "Livré (le "+data.deliveredAt.date.split(".")[0].replace("T"," ")+")";
    }

    const deliveryMan = newCard.querySelector(".deliveryMan");
    deliveryMan.classList.remove(["deliveryMan"]);
    deliveryMan.innerText = data.status === "paid" ? "Encore aucun livreur" : "Livreur : "+data.deliveryManFirstname+" "+data.deliveryManLastname;

    return newCard;
};

const displayCards = (datas) => {
    const container = document.getElementById("commands");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    if (datas.length > 0) {
        datas.forEach(data => {
            container.appendChild(buildCard(data));
        });
    } else {
        const nothing = document.createElement("span");
        nothing.appendChild(document.createTextNode("Aucune commande ne correspond à votre recherche"));
        container.appendChild(nothing);
    }
};

document.getElementById('delivery_man').addEventListener('change', ({target}) => {
    deliveryMan = target.value;
    fetchData();
});

document.getElementById('owner').addEventListener('change', ({target}) => {
    owner = target.value;
    fetchData();
});