<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use App\Repository\MarketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MarketRepository::class)
 */
class Market
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="markets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity=Seed::class, inversedBy="markets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $seed;

    /**
     * @ORM\Column(type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $canSeparate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $minimumQuantity;

    /**
     * @ORM\Column(type="float")
     */
    private $initialQuantity;

    /**
     * @ORM\OneToMany(targetEntity=ItemCommand::class, mappedBy="market")
     */
    private $itemCommands;

    public function __construct()
    {
        $this->itemCommands = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getSeed(): ?Seed
    {
        return $this->seed;
    }

    public function setSeed(?Seed $seed): self
    {
        $this->seed = $seed;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCanSeparate(): ?bool
    {
        return $this->canSeparate;
    }

    public function setCanSeparate(bool $canSeparate): self
    {
        $this->canSeparate = $canSeparate;

        return $this;
    }

    public function getMinimumQuantity(): ?float
    {
        return $this->minimumQuantity;
    }

    public function setMinimumQuantity(?float $minimumQuantity): self
    {
        $this->minimumQuantity = $minimumQuantity;

        return $this;
    }

    public function getInitialQuantity(): ?int
    {
        return $this->initialQuantity;
    }

    public function setInitialQuantity(int $initialQuantity): self
    {
        $this->initialQuantity = $initialQuantity;

        return $this;
    }

    /**
     * @return Collection|ItemCommand[]
     */
    public function getItemCommands(): Collection
    {
        return $this->itemCommands;
    }

    public function addItemCommand(ItemCommand $itemCommand): self
    {
        if (!$this->itemCommands->contains($itemCommand)) {
            $this->itemCommands[] = $itemCommand;
            $itemCommand->setMarket($this);
        }

        return $this;
    }

    public function removeItemCommand(ItemCommand $itemCommand): self
    {
        if ($this->itemCommands->contains($itemCommand)) {
            $this->itemCommands->removeElement($itemCommand);
            // set the owning side to null (unless already changed)
            if ($itemCommand->getMarket() === $this) {
                $itemCommand->setMarket(null);
            }
        }

        return $this;
    }

    /**
     * Transform to string
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }
}
