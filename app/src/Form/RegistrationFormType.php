<?php

namespace App\Form;

use App\Custom\Validator\Constraints\ComplexityPassword;
use App\Custom\Validator\Constraints\ConfirmPassword;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('firstname')
            ->add('lastname')
            ->add('password', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez rentrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit faire minimum {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                    new ComplexityPassword([
                        'message' => "Votre mot de passe n'est pas assez complexe"
                    ])
                ],
            ])
            ->add('confirm_password', PasswordType::class, array(
                'mapped' => false,
                'constraints' => array(
                    new NotBlank(),
                    new ConfirmPassword([
                        'message' => "Les deux mots de passe rentrés ne correspondent pas"
                    ]),
                ),
            ))
            ->add('imageFile', VichImageType::class, [
                "allow_delete" => true,
                "download_uri" => true,
                "image_uri" => true,
                "required" => false,

                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => "La photo de profil doit être des types suivants : .jpeg, .jpg ou .png"
                    ])
                ]
            ])
            ;
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
