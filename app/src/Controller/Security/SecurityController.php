<?php

namespace App\Controller\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\MailService;
use App\Form\ResetPasswordType;
use App\Form\RegistrationFormType;
use App\Service\ResetPasswordService;
use App\Security\AppCustomAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\ValidationTokenService;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param AppCustomAuthenticator $authenticator
     * @param MailService $mailer
     * @return Response
     */
    public function register(Request $request, MailService $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(["ROLE_USER"]);
            $user->setPlainPassword($form->get('password')->getData());
            $user->setRegisterToken(ValidationTokenService::generateToken());
            $mail = $form->get('email')->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email
            $mailer->sendTemplateMail($mail, 'Bienvenue chez Seedseller', 'registration.html.twig', [
                'validationLink' => $this->generateUrl('app_confirm', ['token' => $user->getRegisterToken()], UrlGeneratorInterface::ABSOLUTE_URL)
            ]);

            return $this->redirectToRoute("register_successfull");
        }

        return $this->render('security/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register/successfull", name="register_successfull")
     */
    public function successfull() {
        return $this->render("security/account_to_confirm.html.twig");
    }

    /**
     * @Route("/confirm/{token}", name="app_confirm")
     */
    public function confirm($token, UserRepository $userRepository) {
        $user = $userRepository->findByRegisterToken($token);
        if ($user) {
            $user->setRegisterToken(null);
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->render('security/account_confirm.html.twig', [
            'success' => $user != null
        ]);
    }

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser() && in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
             return $this->redirectToRoute('admin_default_index');
        } elseif ($this->getUser()) {
            return $this->redirectToRoute('default_index');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/reset_password", name="reset_password")
     */
    public function resetPasswordForm(Request $request, UserPasswordEncoderInterface $passwordEncoder, MailService $mailer)
    {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $userRepository = $entityManager->getRepository(User::class);
            $email = $form->get('email')->getData();
            $user = $userRepository->findOneUserbyEmail($email);
            
            if($user == null){
                $this->addFlash('error', "Aucun compte n'est attaché à cette adresse mail");
                return $this->redirectToRoute('reset_password');
            }

            $newPassword = ResetPasswordService::generateRandomPassword();
            $user->setPlainPassword($newPassword);
            $user->setPassword($newPassword);
            $entityManager->flush();

            $mailer->sendTemplateMail($email, 'Votre nouveau mot de passe!',
                                'reset_password.html.twig', [ 'newPwd' => $newPassword ]);
            $this->addFlash('success', "Un mail a été envoyé à cette adresse mail");
        }

        return $this->render('security/reset_password.html.twig', [
            'resetPasswordForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
