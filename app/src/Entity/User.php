<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Serializable;
use App\Entity\Traits\TimestampableTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="user_account")
 * @UniqueEntity(fields={"email"}, message="Un compte avec cet email existe déjà.")
 * @Vich\Uploadable
 */
class User implements UserInterface, Serializable
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    private $plainPassword;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lastname;

    /**
     * @ORM\OneToMany(targetEntity=Addresses::class, mappedBy="customer")
     */
    private $addresses;

    /**
     * @ORM\OneToMany(targetEntity=Market::class, mappedBy="owner")
     */
    private $markets;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="profile_pic", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity=Command::class, mappedBy="owner")
     */
    private $commands;

    /**
     * @ORM\OneToMany(targetEntity=Command::class, mappedBy="delivery_man")
     */
    private $commandsToDelivery;
    
    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $request_delivery = 0;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $registerToken;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->commands = new ArrayCollection();
        $this->commandsToDelivery = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        if (!in_array('ROLE_USER', $roles)){
            // guarantee every user at least has ROLE_USER
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(string $role): self
    {
        $roles = $this->getRoles();

        if (!in_array($role, $roles)){
            $roles[] = $role;
            $this->setRoles($roles);
        }

        return $this;
    }

    public function removeRole(string $roleToRemove): self
    {        
        $this->setRoles(array_filter($this->getRoles(), function($role) use($roleToRemove){
            return $role != $roleToRemove;
        }));

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPlainPassword(): string
    {
        return (string) $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return Collection|Addresses[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Addresses $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setCustomer($this);
        }

        return $this;
    }

    public function removeAddress(Addresses $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getCustomer() === $this) {
                $address->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Market[]
     */
    public function getMarkets(): Collection
    {
        return $this->markets;
    }

    public function addMarket(Market $market): self
    {
        if (!$this->markets->contains($market)) {
            $this->markets[] = $market;
            $market->setOwner($this);
        }

        return $this;
    }

    public function removeMarket(Market $market): self
    {
        if ($this->markets->contains($market)) {
            $this->markets->removeElement($market);
            // set the owning side to null (unless already changed)
            if ($market->getOwner() === $this) {
                $market->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * Transform to string
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getId();
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
    
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->roles,
            $this->password,
            $this->firstname,
            $this->lastname,
            $this->addresses,
            $this->markets,
            $this->image,
        ]);
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->roles,
            $this->password,
            $this->firstname,
            $this->lastname,
            $this->addresses,
            $this->markets,
            $this->image,
        ) = unserialize($serialized);
    }

    /**
     * @return Collection|Command[]
     */
    public function getCommands(): Collection
    {
        return $this->commands;
    }

    public function addCommand(Command $command): self
    {
        if (!$this->commands->contains($command)) {
            $this->commands[] = $command;
            $command->setOwner($this);
        }

        return $this;
    }

    public function removeCommand(Command $command): self
    {
        if ($this->commands->removeElement($command)) {
            // set the owning side to null (unless already changed)
            if ($command->getOwner() === $this) {
                $command->setOwner(null);
            }
        }
    }

    public function getrequest_delivery(): ?int
    {
        return $this->request_delivery;
    }

    public function setrequest_delivery(int $request_delivery): self
    {
        $this->request_delivery = $request_delivery;

        return $this;
    }

    /**
     * @return Collection|Command[]
     */
    public function getCommandsToDelivery(): Collection
    {
        return $this->commandsToDelivery;
    }

    public function addCommandToDelivery(Command $commandsToDelivery): self
    {
        if (!$this->commandsToDelivery->contains($commandsToDelivery)) {
            $this->commandsToDelivery[] = $commandsToDelivery;
            $commandsToDelivery->setOwner($this);
        }

        return $this;
    }

    public function removeCommandToDelivery(Command $commandsToDelivery): self
    {
        if ($this->commandsToDelivery->removeElement($commandsToDelivery)) {
            // set the owning side to null (unless already changed)
            if ($commandsToDelivery->getOwner() === $this) {
                $commandsToDelivery->setOwner(null);
            }
        }
    }

    public function getRegisterToken(): ?string
    {
        return $this->registerToken;
    }

    public function setRegisterToken(?string $registerToken): self
    {
        $this->registerToken = $registerToken;

        return $this;
    }

    public function getCart()
    {
        foreach ($this->getCommands() as $command) {
            if ($command->getStatus() == Command::PENDING_STATUS) return $command;
        }
        return null;
    }
}
