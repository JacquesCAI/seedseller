<?php

namespace App\Entity;

use App\Repository\SeedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=SeedRepository::class)
 * @Vich\Uploadable
 */
class Seed
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="seeds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="seed_pic", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $measurementUnit;

    /**
     * @ORM\OneToMany(targetEntity=Market::class, mappedBy="seed", orphanRemoval=true)
     */
    private $markets;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lifting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberSeedsPerGram;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $plantingDensity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cultivationPractice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diseasesandMainPests;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sowingdate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $harvestdate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $additionalInformation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->markets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getMeasurementUnit(): ?string
    {
        return $this->measurementUnit;
    }

    public function setMeasurementUnit(string $measurementUnit): self
    {
        $this->measurementUnit = $measurementUnit;

        return $this;
    }

    /**
     * @return Collection|Market[]
     */
    public function getMarkets(): Collection
    {
        return $this->markets;
    }

    public function addMarket(Market $market): self
    {
        if (!$this->markets->contains($market)) {
            $this->markets[] = $market;
            $market->setSeed($this);
        }

        return $this;
    }

    public function removeMarket(Market $market): self
    {
        if ($this->markets->removeElement($market)) {
            // set the owning side to null (unless already changed)
            if ($market->getSeed() === $this) {
                $market->setSeed(null);
            }
        }

        return $this;
    }

    /**
     * Transform to string
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    public function getLifting(): ?string
    {
        return $this->lifting;
    }

    public function setLifting(?string $lifting): self
    {
        $this->lifting = $lifting;

        return $this;
    }

    public function getNumberSeedsPerGram(): ?int
    {
        return $this->numberSeedsPerGram;
    }

    public function setNumberSeedsPerGram(?int $numberSeedsPerGram): self
    {
        $this->numberSeedsPerGram = $numberSeedsPerGram;

        return $this;
    }

    public function getPlantingDensity(): ?int
    {
        return $this->plantingDensity;
    }

    public function setPlantingDensity(?int $plantingDensity): self
    {
        $this->plantingDensity = $plantingDensity;

        return $this;
    }

    public function getCultivationPractice(): ?string
    {
        return $this->cultivationPractice;
    }

    public function setCultivationPractice(?string $cultivationPractice): self
    {
        $this->cultivationPractice = $cultivationPractice;

        return $this;
    }

    public function getDiseasesandMainPests(): ?string
    {
        return $this->diseasesandMainPests;
    }

    public function setDiseasesandMainPests(?string $diseasesandMainPests): self
    {
        $this->diseasesandMainPests = $diseasesandMainPests;

        return $this;
    }

    public function getSowingdate(): ?string
    {
        return $this->sowingdate;
    }

    public function setSowingdate(?string $sowingdate): self
    {
        $this->sowingdate = $sowingdate;

        return $this;
    }

    public function getHarvestdate(): ?string
    {
        return $this->harvestdate;
    }

    public function setHarvestdate(?string $harvestdate): self
    {
        $this->harvestdate = $harvestdate;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
