<?php

namespace App\Service;

use App\Entity\ItemCommand;
use App\Entity\Market;

class CommandService {

    public function updateMarket(ItemCommand $command) {
        $market = $command->getMarket();
        if($market->getQuantity() > $command->getQuantity()) {
            $newQuantity = $market->getQuantity() - $command->getQuantity();
            $market->setQuantity($newQuantity);
        } else {
            $market->setQuantity(0);
        }
    }

    public function getDefaultQuantity(Market $market) {
        return $market->getMinimumQuantity() ? min($market->getQuantity(),$market->getMinimumQuantity()) : $market->getQuantity();
    }

    public function getRangebarStep(Market $market, ItemCommand $itemCommand = null) {

        $numberFigureAfterComma = 0;

        $quantity = $itemCommand == null ? strval($market->getQuantity()) : strval($market->getQuantity()+$itemCommand->getQuantity());
        $quantitySplit = explode('.', $quantity);

        $minimumQuantity = strval($market->getMinimumQuantity());
        $minimumQuantitySplit = explode('.', $minimumQuantity);

        $finalQuantityArr = count($quantitySplit) > count($minimumQuantitySplit) ? $quantitySplit : $minimumQuantitySplit;

        if(count($finalQuantityArr) > 1) {
            $numberFigureAfterComma = strlen(end($finalQuantityArr));
        }

        return 1/pow(10, $numberFigureAfterComma);
    }
}