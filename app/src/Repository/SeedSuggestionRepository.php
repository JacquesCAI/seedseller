<?php

namespace App\Repository;

use App\Entity\SeedSuggestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SeedSuggestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeedSuggestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeedSuggestion[]    findAll()
 * @method SeedSuggestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeedSuggestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeedSuggestion::class);
    }

    public function findUnvalidateds() {
        return $this->createQueryBuilder("s")
            ->where("s.validated = false")
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return SeedSuggestion[] Returns an array of SeedSuggestion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SeedSuggestion
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
