<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

class ConfirmPassword extends Constraint
{
    public $message = 'Your passwords does not match';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }

//    public function getTargets() {
//        return self::CLASS_CONSTRAINT;
//    }
}