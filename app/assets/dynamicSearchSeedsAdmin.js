
let searchStr = "";
let categoryId = null;

const fetchData = () => {

    const data = {
        category: categoryId,
        search: searchStr
    };

    fetch("/seed/search", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(data => displayCards(data));

};
const buildCard = (data) => {
    const prototype = document.getElementById("card-prototype");

    const newCard = prototype.cloneNode(true);
    newCard.removeAttribute('id');
    newCard.classList.remove(['d-none']);

    newCard.querySelector('.custom-card-head img').src = "/uploads/images/seed_pic/"+data.image;
    newCard.querySelector("h3").innerText = data.name;
    newCard.querySelector("p").innerText = data.category;
    newCard.querySelector(".nbOffers span").innerText = data.nbOffers + ' offres';
    newCard.querySelector(".avgPrice span").innerText = data.avgPrice != null ? (parseFloat(data.avgPrice).toFixed(2) + '€/' + data.measurementUnit) : "encore aucune offre";
    newCard.querySelector(".show-seed").href = (backoffice ? '/admin/market/list/'+data.id : '/seed/' + data.id);
    if (backoffice) {
        newCard.querySelector(".edit-seed").href = '/admin/seed/' + data.id+'/edit';
    }
    newCard.querySelector('.more-infos').href = (backoffice ? '/admin' : '')+'/seed/'+data.id+"/more-information";

    return newCard;
};

const displayCards = (datas) => {
    const container = document.getElementById("cards");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    if (datas.length > 0) {
        datas.forEach(data => {
            const card = buildCard(data);
            container.appendChild(card);
        });
    } else {
        const nothingSeed = document.createElement("span");
        nothingSeed.appendChild(document.createTextNode("Aucune graine ne correspond à votre recherche"));
        container.appendChild(nothingSeed);
    }
};

const searchField = document.getElementById('search');
const categoriesRadio = document.querySelectorAll("input[type=radio]")

searchField.addEventListener('input', ({target}) => {
    searchStr = target.value;
    fetchData();
});

categoriesRadio.forEach(radio =>
    radio.addEventListener('click', ({target}) => {
        if (target.getAttribute('data-isCheck')) {
            categoryId = null;
            target.checked = false;
            target.removeAttribute('data-isCheck');
        } else {
            target.setAttribute("data-isCheck", "true");
            categoryId = target.value;
        }
        fetchData();
    })
)