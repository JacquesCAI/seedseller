<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210714151700 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE addresses_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE command_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE item_command_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE market_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE seed_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE seed_suggestion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE addresses (id INT NOT NULL, customer_id INT NOT NULL, city VARCHAR(100) NOT NULL, zipcode INT NOT NULL, address VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6FCA75169395C3F3 ON addresses (customer_id)');
        $this->addSql('CREATE TABLE category (id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE command (id INT NOT NULL, address_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, delivery_man_id INT DEFAULT NULL, status VARCHAR(255) NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8ECAEAD4F5B7AF75 ON command (address_id)');
        $this->addSql('CREATE INDEX IDX_8ECAEAD47E3C61F9 ON command (owner_id)');
        $this->addSql('CREATE INDEX IDX_8ECAEAD4FD128646 ON command (delivery_man_id)');
        $this->addSql('CREATE TABLE item_command (id INT NOT NULL, market_id INT NOT NULL, command_id INT DEFAULT NULL, quantity DOUBLE PRECISION NOT NULL, price DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8597CC80622F3F37 ON item_command (market_id)');
        $this->addSql('CREATE INDEX IDX_8597CC8033E1689A ON item_command (command_id)');
        $this->addSql('CREATE TABLE market (id INT NOT NULL, owner_id INT NOT NULL, seed_id INT NOT NULL, quantity DOUBLE PRECISION NOT NULL, price DOUBLE PRECISION NOT NULL, can_separate BOOLEAN NOT NULL, minimum_quantity DOUBLE PRECISION DEFAULT NULL, initial_quantity DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6BAC85CB7E3C61F9 ON market (owner_id)');
        $this->addSql('CREATE INDEX IDX_6BAC85CB64430F6A ON market (seed_id)');
        $this->addSql('CREATE TABLE seed (id INT NOT NULL, category_id INT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(1024) DEFAULT NULL, measurement_unit VARCHAR(30) NOT NULL, lifting VARCHAR(255) DEFAULT NULL, number_seeds_per_gram INT DEFAULT NULL, planting_density INT DEFAULT NULL, cultivation_practice TEXT DEFAULT NULL, diseasesand_main_pests VARCHAR(255) DEFAULT NULL, sowingdate VARCHAR(255) DEFAULT NULL, harvestdate VARCHAR(255) DEFAULT NULL, additional_information TEXT DEFAULT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4487E30612469DE2 ON seed (category_id)');
        $this->addSql('CREATE TABLE seed_suggestion (id INT NOT NULL, category_name_id INT NOT NULL, image VARCHAR(1024) DEFAULT NULL, measurement_unit VARCHAR(30) NOT NULL, suggestor_firstname VARCHAR(255) NOT NULL, suggestion_lastname VARCHAR(255) NOT NULL, suggestor_email VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, additional_information TEXT DEFAULT NULL, validated BOOLEAN NOT NULL, seed_name VARCHAR(255) NOT NULL, lifting VARCHAR(255) DEFAULT NULL, number_seeds_per_gram INT DEFAULT NULL, planting_density INT DEFAULT NULL, cultivation_practice TEXT DEFAULT NULL, diseases_and_main_pests VARCHAR(255) DEFAULT NULL, sowingdate VARCHAR(255) DEFAULT NULL, harvestdate VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2548DEC3B6CFDCA8 ON seed_suggestion (category_name_id)');
        $this->addSql('CREATE TABLE user_account (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(50) NOT NULL, lastname VARCHAR(50) NOT NULL, image VARCHAR(255) DEFAULT NULL, request_delivery SMALLINT DEFAULT 0 NOT NULL, register_token VARCHAR(50) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT \'NOW()\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_253B48AEE7927C74 ON user_account (email)');
        $this->addSql('ALTER TABLE addresses ADD CONSTRAINT FK_6FCA75169395C3F3 FOREIGN KEY (customer_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE command ADD CONSTRAINT FK_8ECAEAD4F5B7AF75 FOREIGN KEY (address_id) REFERENCES addresses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE command ADD CONSTRAINT FK_8ECAEAD47E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE command ADD CONSTRAINT FK_8ECAEAD4FD128646 FOREIGN KEY (delivery_man_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_command ADD CONSTRAINT FK_8597CC80622F3F37 FOREIGN KEY (market_id) REFERENCES market (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE item_command ADD CONSTRAINT FK_8597CC8033E1689A FOREIGN KEY (command_id) REFERENCES command (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE market ADD CONSTRAINT FK_6BAC85CB7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE market ADD CONSTRAINT FK_6BAC85CB64430F6A FOREIGN KEY (seed_id) REFERENCES seed (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE seed ADD CONSTRAINT FK_4487E30612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE seed_suggestion ADD CONSTRAINT FK_2548DEC3B6CFDCA8 FOREIGN KEY (category_name_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE EXTENSION unaccent');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE command DROP CONSTRAINT FK_8ECAEAD4F5B7AF75');
        $this->addSql('ALTER TABLE seed DROP CONSTRAINT FK_4487E30612469DE2');
        $this->addSql('ALTER TABLE seed_suggestion DROP CONSTRAINT FK_2548DEC3B6CFDCA8');
        $this->addSql('ALTER TABLE item_command DROP CONSTRAINT FK_8597CC8033E1689A');
        $this->addSql('ALTER TABLE item_command DROP CONSTRAINT FK_8597CC80622F3F37');
        $this->addSql('ALTER TABLE market DROP CONSTRAINT FK_6BAC85CB64430F6A');
        $this->addSql('ALTER TABLE addresses DROP CONSTRAINT FK_6FCA75169395C3F3');
        $this->addSql('ALTER TABLE command DROP CONSTRAINT FK_8ECAEAD47E3C61F9');
        $this->addSql('ALTER TABLE command DROP CONSTRAINT FK_8ECAEAD4FD128646');
        $this->addSql('ALTER TABLE market DROP CONSTRAINT FK_6BAC85CB7E3C61F9');
        $this->addSql('DROP SEQUENCE addresses_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE category_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE command_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE item_command_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE market_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE seed_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE seed_suggestion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_account_id_seq CASCADE');
        $this->addSql('DROP TABLE addresses');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE command');
        $this->addSql('DROP TABLE item_command');
        $this->addSql('DROP TABLE market');
        $this->addSql('DROP TABLE seed');
        $this->addSql('DROP TABLE seed_suggestion');
        $this->addSql('DROP TABLE user_account');
        $this->addSql('DROP EXTENSION unaccent');
    }
}
