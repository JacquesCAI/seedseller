<?php

namespace App\DataFixtures;

use App\Entity\Command;
use App\Entity\ItemCommand;
use App\Entity\Market;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ItemCommandFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $commands = $manager->getRepository(Command::class)->findAll();
        $markets = $manager->getRepository(Market::class)->findAll();

        for ($i = 0; $i < 50; $i++) {

            $market = $markets[array_rand($markets)];

            $itemCommand = (new ItemCommand())
                ->setQuantity($market->getQuantity())
                ->setMarket($market)
                ->setPrice($market->getPrice() * $market->getQuantity())
                ->setCommand($commands[array_rand($commands)])
            ;

            $manager->persist($itemCommand);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CommandFixtures::class,
            MarketFixtures::class
        ];
    }

}
