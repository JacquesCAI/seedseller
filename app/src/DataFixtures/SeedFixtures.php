<?php

namespace App\DataFixtures;

use App\Entity\Seed;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class SeedFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        $categories = $manager->getRepository(Category::class)->findAll();
        $seedsName = [
            "Moutarde", "Chia", "Pavot", "Courge", "Tournesol", "Sésame",
            "Lin", "Nigelle", "Anis", "Fenouil", "Chanvre", "Citrouille", "Aubergine",
            "Carotte", "Betterave", "Pomme de terre", "Radis", "Tomate", "Maïs", "Blé",
            "Courgette", "Céleri", "Avoine", "Citron", "Kiwi"];

        $images = ["seed_picture1.jpeg", "seed_picture2.jpeg", "seed_picture3.jpeg", "seed_picture4.jpeg",
            "seed_picture5.jpeg", "seed_picture6.jpeg"];

        for ($i = 0; $i < count($seedsName); $i++) {
            $seed = (new Seed())
                ->setName($seedsName[$i])
                ->setImage($images[array_rand($images)])
                ->setCategory($categories[array_rand($categories)])
                ->setMeasurementUnit('kg')
                ->setDescription($faker->text)
                ->setLifting($faker->text)
                ->setNumberSeedsPerGram($faker->numberBetween(1, 1000))
                ->setPlantingDensity($faker->numberBetween(1, 100))
                ->setSowingdate($faker->monthName)
                ->setHarvestdate($faker->monthName)
                ->setCultivationPractice($faker->text)
                ->setDiseasesandMainPests($faker->text)
                ->setAdditionalInformation($faker->text)
            ;
            $manager->persist($seed);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }

}
