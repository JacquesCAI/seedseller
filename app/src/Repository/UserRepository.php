<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    public function findOneUserbyEmail($email)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :email')
            ->setParameter('email', $email)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findBySearchAndRole($search, $role) {
        $keyWords = array_filter(explode(' ', $search), function ($keyword) {
            return $keyword != "";
        });
        $request = $this->createQueryBuilder('u')
            ->select("u.id, u.roles, u.firstname, u.lastname, u.email, u.image, count(distinct m) as nbMarkets, count(distinct c) as nbCommands, count(distinct cd) as nbCommandsToDelivery")
            ->leftJoin("u.markets", "m")
            ->leftJoin("u.commands", "c")
            ->leftJoin("u.commandsToDelivery", "cd")
            ;

        for ($i=0;$i<count($keyWords);$i++) {
            if ($i == 0) {
                $request->where("UNACCENT(LOWER(u.firstname)) LIKE UNACCENT(LOWER(:keyword".$i."))");
            } else {
                $request->orWhere("UNACCENT(LOWER(u.firstname)) LIKE UNACCENT(LOWER(:keyword".$i."))");
            }
            $request
                ->orWhere("UNACCENT(LOWER(u.lastname)) LIKE UNACCENT(LOWER(:keyword".$i."))")
                ->orWhere("UNACCENT(LOWER(u.email)) LIKE UNACCENT(LOWER(:keyword".$i."))")
            ;
        }

        for ($i=0;$i<count($keyWords);$i++) {
            $request->setParameter('keyword'.$i, '%'.$keyWords[$i].'%');
        }

        $users = $request->groupBy("u.id")
            ->getQuery()
            ->getResult();
        $users_filtered = [];
        foreach($users as $user){
            
            if( in_array($role, $user['roles'])){
                $users_filtered[] = $user;
            }
        }
        return $users_filtered;
    }

    public function findByRequestDeliveryman($search) {
        return $this->createQueryBuilder('u')
            ->select("u.id, u.firstname, u.lastname, u.email, u.request_delivery, u.image, count(distinct m) as nbMarkets, count(distinct c) as nbCommands")
            ->leftJoin("u.markets", "m")
            ->leftJoin("u.commands", "c")
            ->where("u.firstname LIKE :search")
            ->orWhere("u.lastname LIKE :search")
            ->orWhere("u.email LIKE :search")
            ->andWhere("u.request_delivery = 2")
            ->setParameter("search", "%".$search."%")
            ->groupBy("u.id")
            ->getQuery()
            ->getResult();
    }

    public function findByRegisterToken($registerToken) {
        return $this->createQueryBuilder('u')
            ->where('u.registerToken = :registerToken')
            ->setParameter('registerToken', $registerToken)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
