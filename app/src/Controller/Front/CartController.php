<?php

namespace App\Controller\Front;

use App\Entity\Command;
use App\Entity\ItemCommand;
use App\Entity\Market;
use App\Form\CommandType;
use App\Form\ItemCommandType;
use App\Repository\CommandRepository;
use App\Service\CommandService;
use App\Service\ValidationTokenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Security\Voter\CartVoter;
use App\Security\Voter\MarketVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Stripe\Stripe;

// secret_key = sk_test_51H1Z9XA4txFW05KB8be30rFW09MFZzi0rgt0Hzcw54GapPnqSvCayCLnvuDdYGkq8r8fh6H9mkwudyMOE5HEbaOk00dpPHUd2F
// public_key = pk_test_51H1Z9XA4txFW05KBDkgEF4GHU7CVAovpIFXeGSex5F97uDlLtDxkhc7SSdxNJ6Au6jvxpS9vfpRp38NG4Kpb5Ldh00URrLJGQx

/**
 * @Route("/cart")
 */

class CartController extends AbstractController
{
    /**
     * @Route("/", name="cart_index")
     */
    public function index(CommandRepository $commandRepository, CommandService $commandService): Response
    {
        $cart = $commandRepository->findPendingCommandByOwnerId($this->getUser()->getId());
        return $this->render('front/cart/index.html.twig', [
            "cart" => $cart
        ]);
    }

    /**
     * @Route("/confirm_payment/{token}", name="confirm_payment")
     */
    public function confirm_payment(CommandRepository $commandRepository, $token)
    {
        
        $session = new Session();
        if($session->get('token') == $token){
            $cart = $commandRepository->findPendingCommandByOwnerId($this->getUser()->getId());
            $cart->setStatus(Command::PAID_STATUS);
            $this->getDoctrine()->getManager()->flush();
            $session->remove('token');
            return $this->render('front/cart/confirmpayment.html.twig');
            
        }
        $this->addFlash("error", "Le paiement n'a pas fonctionné");
        return $this->redirectToRoute('cart_index');
    }

    /**
     * @Route("/cancel_payment", name="cancel_payment")
     */
    public function cancel_payment()
    {
        $this->addFlash('error', "Le paiement n'a pas fonctionné");
        return $this->redirectToRoute('cart_index');

    }

    /**
     * @Route("/payement", name="cart_payment")
     */
    public function payment(CommandRepository $commandRepository) { // This function will not be executed in waiting for stripe working
        $cart = $commandRepository->findPendingCommandByOwnerId($this->getUser()->getId());
        if ($cart == null || count($cart->getCommandItems()) == 0) {
            $this->addFlash("error", "Vous n'avez aucun panier à payer");
            return $this->redirectToRoute('cart_index');
        }
        if ($cart->getAddress() == null) {
            $this->addFlash("error", "Vous devez définir une adresse");
            return $this->redirectToRoute('cart_index');
        }

        $items = [];
        foreach ($cart->getCommandItems() as $item) {
            $items[] = $item;
        }
        $token = ValidationTokenService::generateToken();
        $session = new Session();
        $session->set('token', $token);
        Stripe::setApiKey('sk_test_51H1Z9XA4txFW05KB8be30rFW09MFZzi0rgt0Hzcw54GapPnqSvCayCLnvuDdYGkq8r8fh6H9mkwudyMOE5HEbaOk00dpPHUd2F');
        $obj = [
            'payment_method_types' => ['card'],
            'line_items' => array_map(function ($item) {
                return [
                    'price_data' => [
                        'currency' => 'eur',
                        'product_data' => [
                            'name' => $item->getMarket()->getSeed()->getName()
                        ],
                        'unit_amount' => round($item->getPrice()*100)
                    ],
                    'quantity' => /*$item->getQuantity()*/1
                ];
            }, $items),
            'mode' => 'payment',
            'success_url' => $this->generateUrl('confirm_payment', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL), // Keep default redirection urls, in waiting for stripe working
            'cancel_url' => $this->generateUrl('cancel_payment', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ];
        $session = \Stripe\Checkout\Session::create($obj);
        return $this->render("front/cart/payment.html.twig", [
            'sessionId' => $session->id
        ]);
    }

    /**
     * @Route("/valid", name="cart_valid")
     */
    public function valid(CommandRepository $commandRepository, Request $request): Response
    {
        $cart = $commandRepository->findPendingCommandByOwnerId($this->getUser()->getId());
        if ($cart == null || count($cart->getCommandItems()) == 0) {
            $this->addFlash("error", "Vous n'avez aucun panier à valider");
            return $this->redirectToRoute('cart_index');
        }
        $form = $this->createForm(CommandType::class, $cart, [
            'owner' => $this->getUser(),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cart_payment');
        }

        return $this->render('front/cart/valid.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/new", name="cart_new", methods={"GET","POST"})
     * @IsGranted(MarketVoter::BUY, subject="market")
     */
    public function new(Market $market, Request $request, CommandService $cs, CommandRepository $cr): Response
    {
        foreach ($market->getItemCommands() as $itemCommand) {
            if ($itemCommand->getCommand()->getStatus() == Command::PENDING_STATUS && $itemCommand->getCommand()->getOwner()->getId() == $this->getUser()->getId()) {
                throw new Exception("Vous avez déjà cette offre dans votre panier");
            }
        }
        $commandItem = new ItemCommand();
        $form = $this->createForm(ItemCommandType::class, $commandItem, [
            'market' => $market
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if (!$market->getCanSeparate())
                $commandItem->setQuantity($market->getQuantity());
            $commandItem->setPrice(round($commandItem->getQuantity()*$market->getPrice(),2));
            $commandItem->setCreatedAt(new \DateTime());

            $cs->updateMarket($commandItem);
            $cart = $cr->findPendingCommandByOwnerId($this->getUser()->getId());
            if($cart == null)
            {
                $cart = new Command();
                $cart->setStatus(Command::PENDING_STATUS);
                $cart->setOwner($this->getUser());
                $entityManager->persist($cart);;
                $entityManager->flush();
            }
            $commandItem->setCommand($cart);
            $entityManager->persist($commandItem);
            $entityManager->flush();

            $this->addFlash('success', 'Article aujouté au panier!');

            return $this->redirectToRoute('seed_show', ["id" => $market->getSeed()->getId()]);
        }

        $defaultQuantity = $cs->getDefaultQuantity($market);

        return $this->render('front/cart/new_or_edit.html.twig', [
            'market' => $market,
            'form' => $form->createView(),
            'step' => $cs->getRangebarStep($market),
            'defaultQuantity' => $defaultQuantity,
            'valueQuantity' => $defaultQuantity,
            'quantity' => $market->getQuantity(),
            'new' => true
        ]);
    }

    /**
     * @Route("/{id}", name="cart_delete", methods={"DELETE"})
     * @IsGranted(CartVoter::DELETE, subject="itemCommand")
     */
    public function delete(Request $request, ItemCommand $itemCommand): Response
    {
        if ($this->isCsrfTokenValid('delete'.$itemCommand->getId(), $request->request->get('_token'))) {
            $market = $itemCommand->getMarket();
            $market->setQuantity($market->getQuantity()+$itemCommand->getQuantity());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($itemCommand);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cart_index');
    }

    /**
     * @Route("/{id}/edit", name="cart_edit", methods={"GET","POST"})
     * @IsGranted(CartVoter::EDIT, subject="commandItem")
     */
    public function edit(Request $request, ItemCommand $commandItem, CommandService $cs): Response
    {
        $market = $commandItem->getMarket();
        $commandItem->setMarket(null);
        $form = $this->createForm(ItemCommandType::class, $commandItem, [
            'market' => $market
        ]);
        $oldQuantity = $commandItem->getQuantity();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $market->setQuantity(round($market->getQuantity()+($oldQuantity-$commandItem->getQuantity()),2));
            $commandItem->setPrice(round($commandItem->getQuantity()*$market->getPrice(),2));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cart_index');
        }

        return $this->render('front/cart/new_or_edit.html.twig', [
            'market' => $market,
            'form' => $form->createView(),
            'step' => $cs->getRangebarStep($market,$commandItem),
            'defaultQuantity' => $market->getMinimumQuantity(),
            'quantity' => $market->getQuantity()+$commandItem->getQuantity(),
            'valueQuantity' => $commandItem->getQuantity(),
            'new' => false
        ]);
    }
}
