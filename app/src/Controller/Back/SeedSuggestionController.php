<?php

namespace App\Controller\Back;

use App\Entity\Seed;
use App\Entity\SeedSuggestion;
use App\Form\SeedSuggestionType;
use App\Repository\SeedSuggestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Security\Voter\SeedSuggestionVoter;

/**
 * @Route("/seed/suggestion")
 */
class SeedSuggestionController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/all", name="seed_suggestion_index", methods={"GET"})
     * @param SeedSuggestionRepository $seedSuggestionRepository
     * @return Response
     */
    public function index(SeedSuggestionRepository $seedSuggestionRepository): Response
    {
        return $this->render('back/seed_suggestion/index.html.twig', [
            'seed_suggestions' => $seedSuggestionRepository->findUnvalidateds(),
            'backoffice' => true
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seed_suggestion_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     * @IsGranted(SeedSuggestionVoter::EDIT, subject="seedSuggestion")
     * @param Request $request
     * @param SeedSuggestion $seedSuggestion
     * @return Response
     */
    public function edit(Request $request, SeedSuggestion $seedSuggestion): Response
    {
        $form = $this->createForm(SeedSuggestionType::class, $seedSuggestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seedSuggestion);
            $entityManager->flush();

            $this->addFlash('success', 'La suggestion '. $seedSuggestion->getSeedName() .' a bien été modifié!');

            return $this->redirectToRoute('admin_seed_suggestion_index');
        }

        return $this->render('back/seed_suggestion/edit.html.twig', [
            'seed_suggestion' => $seedSuggestion,
            'form' => $form->createView(),
            'backoffice' => true
        ]);
    }

    /**
     * @Route("/{id}/validate", name="seed_suggestion_validate", methods={"POST"}, requirements={"id":"\d+"})
     * @IsGranted(SeedSuggestionVoter::VALIDATE, subject="seedSuggestion")
     * @param Request $request
     * @param SeedSuggestion $seedSuggestion
     * @return Response
     */
    public function validate(Request $request, SeedSuggestion $seedSuggestion): Response
    {

        if ($this->isCsrfTokenValid('validate' . $seedSuggestion->getId(), $request->request->get('_token')) && $seedSuggestion->getValidated() == false) {
            $seed = new Seed();

            $seed->setName($seedSuggestion->getSeedName())
                ->setCategory($seedSuggestion->getCategoryName())
                ->setImage($seedSuggestion->getImage())
                ->setMeasurementUnit($seedSuggestion->getMeasurementUnit())
                ->setLifting($seedSuggestion->getLifting())
                ->setNumberSeedsPerGram($seedSuggestion->getNumberSeedsPerGram())
                ->setPlantingDensity($seedSuggestion->getPlantingDensity())
                ->setSowingdate($seedSuggestion->getSowingdate())
                ->setHarvestdate($seedSuggestion->getHarvestdate())
                ->setCultivationPractice($seedSuggestion->getCultivationPractice())
                ->setDiseasesAndMainPests($seedSuggestion->getDiseasesAndMainPests())
                ->setAdditionalInformation($seedSuggestion->getAdditionalInformation())
            ;

            $seedSuggestion->setValidated(true);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seed);
            $entityManager->persist($seedSuggestion);
            $entityManager->flush();

            $this->addFlash("success", "Vous avez bien validé la graine ". $seed->getName() ."!");
        }

        return $this->redirectToRoute('admin_seed_suggestion_index');
    }

    /**
     * @Route("/{id}", name="seed_suggestion_delete", methods={"DELETE"})
     * @IsGranted(SeedSuggestionVoter::DELETE, subject="seedSuggestion")
     * @param Request $request
     * @param SeedSuggestion $seedSuggestion
     * @return Response
     */
    public function delete(Request $request, SeedSuggestion $seedSuggestion): Response
    {
        if ($this->isCsrfTokenValid('delete' . $seedSuggestion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($seedSuggestion);
            $entityManager->flush();

            $this->addFlash('success', 'La suggestion '. $seedSuggestion->getSeedName() .' a bien été refusé et supprimé!');
        }

        return $this->redirectToRoute('admin_seed_suggestion_index');
    }


}
