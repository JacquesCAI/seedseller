<?php

namespace App\Repository;

use App\Entity\Command;
use App\Entity\ItemCommand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Command|null find($id, $lockMode = null, $lockVersion = null)
 * @method Command|null findOneBy(array $criteria, array $orderBy = null)
 * @method Command[]    findAll()
 * @method Command[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Command::class);
    }

    // /**
    //  * @return Command[] Returns an array of Command objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Command
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findCommands($owner_id)
    {
        return $this->createQueryBuilder('c')
            ->where("c.status != :pending")
            ->andWhere("c.owner = :owner_id")
            ->setParameter('pending', Command::PENDING_STATUS)
            ->setParameter('owner_id', $owner_id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findNotPendingCommandsBydeliveryManAndOwner($json = false, $delivery_man_id = "all", $owner_id = "all") {
        $request = $this->createQueryBuilder('c');

        if ($json) {
            $request->select('
                o.firstname as ownerFirstname, o.lastname as ownerLastname, o.image as ownerImage,
                dm.firstname as deliveryManFirstname, dm.lastname as deliveryManLastname,
                c.status, c.deliveredAt, SUM(ic.price) as totalPrice, a.city, a.address
            ')
                ->leftJoin('c.owner', 'o')
                ->leftJoin('c.delivery_man', 'dm')
                ->leftJoin('c.address', 'a')
                ->leftJoin(ItemCommand::class, 'ic', Expr\Join::WITH, 'c.id = ic.command')
            ;
        }

        $request->where('c.status != :pending')
            ->setParameter('pending', Command::PENDING_STATUS)
            ;


        if ($delivery_man_id != "all") {
            $request->andWhere("c.delivery_man = :delivery_man")
                ->setParameter('delivery_man', $delivery_man_id)
                ;
        }
        if ($owner_id != "all") {
            $request->andWhere("c.owner = :owner")
                ->setParameter('owner', $owner_id)
            ;
        }
        if ($json) {
            $request->groupBy('o.firstname, o.lastname, o.image,
                dm.firstname, dm.lastname,
                c.id, c.status, c.deliveredAt, a.city, a.address');
        }
        return $request->orderBy('c.id','DESC')->getQuery()->getResult();
    }

    public function findPendingCommandByOwnerId($owner_id)
    {
        return $this->createQueryBuilder('c')
            ->where('c.status = :pending')
            ->andWhere('c.owner = :owner_id')
            ->setParameter('pending', Command::PENDING_STATUS)
            ->setParameter('owner_id', $owner_id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findCommandWithoutDeliveryMan()
    {
        return $this->createQueryBuilder('c')
            ->where ('c.delivery_man is NULL')
            ->andWhere('c.status = :paid')
            ->setParameter('paid', Command::PAID_STATUS)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findDispatchedByDeliveryMan($deliveryman_id)
    {
        return $this->createQueryBuilder('c')
            ->where ('c.delivery_man = :deliveryman_id ')
            ->andWhere('c.status = :dispatched')
            ->setParameter('deliveryman_id', $deliveryman_id)
            ->setParameter('dispatched', Command::DISPATCHED_STATUS)
            ->getQuery()
            ->getResult()
        ;
    }
}
