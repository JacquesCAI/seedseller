<?php

namespace App\Form;

use App\Entity\ItemCommand;
use App\Entity\Market;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemCommandType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('market', HiddenType::class, [
                'empty_data' => $options['market']
            ])
            ->add('quantity', RangeType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ItemCommand::class,
            'market' => new Market()
        ]);

        $resolver->setAllowedTypes('market', Market::class);

    }
}
