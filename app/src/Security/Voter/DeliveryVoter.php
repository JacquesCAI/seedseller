<?php

namespace App\Security\Voter;

use App\Entity\Command;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;

class DeliveryVoter extends Voter {

    const TAKE = 'take';
    const VALID = 'valid';
    const VIEW = 'view_delivery';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::TAKE, self::VALID, self::VIEW])) {
            return false;
        }

        // only vote on `Command` objects
        if (!$subject instanceof Command) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Command object, thanks to `supports()`
        /** @var Command $command */
        $command = $subject;

        switch ($attribute) {
            case self::TAKE:
                return $this->canTake($command, $user);
            case self::VALID:
                return $this->canValid($command, $user);
            case self::VIEW:
                return $this->canView($command, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canTake(Command $command, User $user)
    {
        return $command->getDeliveryMan() === null && $this->security->isGranted('ROLE_DELIVERY') && $command->getStatus() === Command::PAID_STATUS;
    }

    private function canValid(Command $command, User $user)
    {
        return $command->getDeliveryMan()->getId() === $user->getId() && $this->security->isGranted('ROLE_DELIVERY') && $command->getStatus() === Command::DISPATCHED_STATUS;
    }

    private function canView(Command $command, User $user)
    {
        return $command->getDeliveryMan() && $user->getId() == $command->getDeliveryMan()->getId();
    }

}