<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    //PWD = test
    private const PWD = 'testtest';

    public function load(ObjectManager $manager)
    {

        $faker = \Faker\Factory::create('fr_FR');
        $images = ["profile_picture1.jpeg", "profile_picture2.jpeg", "profile_picture3.jpeg", "profile_picture4.jpeg",
            "profile_picture5.jpg", "profile_picture6.jpg","profile_picture7.jpg","profile_picture8.png","profile_picture9.png",
            "profile_picture9.png","profile_picture10.png","profile_picture11.jpeg"];

        $user = (new User())
            ->setEmail('dev@admin')
            ->setRoles(['ROLE_ADMIN','ROLE_USER'])
            ->setPassword(self::PWD)
            ->setPlainPassword(self::PWD)
            ->setFirstname('dev')
            ->setLastname('admin');

        $manager->persist($user);

        $deliveryUser = (new User())
            ->setEmail('dev@delivery')
            ->setRoles(['ROLE_DELIVERY','ROLE_USER'])
            ->setPassword(self::PWD)
            ->setPlainPassword(self::PWD)
            ->setFirstname('dev')
            ->setLastname('delivery');

        $manager->persist($deliveryUser);

        for ($i = 0; $i < 10; $i++) {
            $user = (new User())
                ->setEmail($faker->email)
                ->setImage($images[array_rand($images)])
                ->setRoles(rand(1,4) == 1 ? ['ROLE_USER','ROLE_DELIVERY'] : ['ROLE_USER'])
                ->setPassword(self::PWD)
                ->setPlainPassword(self::PWD)
                ->setFirstname($faker->firstName)
                ->setLastname($faker->lastName)
            ;

            $manager->persist($user);
        }

        $manager->flush();
    }
}
