import './styles/delivery-map.scss'

const delivery_address_geocode = [
    [48.8566969,2.3514616],
    [45.7578137,4.8320114],
    [43.2961743,5.3699525],
    [48.584614,7.7507127],
    [48.3905283,-4.4860088],
    [50.6365654,3.0635282],
    [44.841225,-0.5800364],
    [43.6044622,1.4442469],
    [47.2186371,-1.5541362],
    [43.7009358,7.2683912]
]

var randomNumber = Math.floor(Math.random() * 10)

window.onload = function () {
    let map = L.map("delivery_map").setView([51.5073509, -0.1277583], 10)
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        attribution: 'SeedSeller',
        minZoom: 1,
        maxZoom: 20,
    }).addTo(map);

    L.Routing.control({
        waypoints: [
            L.latLng(47.7435803,7.3644123),
            L.latLng(delivery_address_geocode[randomNumber])
        ],

        router: new L.Routing.osrmv1({
            language: 'fr',
            profile: 'car',
        })
    }).addTo(map)
}