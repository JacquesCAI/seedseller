<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

use App\Entity\Market;
use App\Entity\User;

class MarketVoter extends Voter {

    const VIEW = 'view';
    const EDIT = 'edit';
    const CLOSE = 'close';
    const DELETE = 'delete';
    const BUY = 'buy';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::CLOSE, self::DELETE, self::BUY])) {
            return false;
        }

        // only vote on `Market` objects
        if (!$subject instanceof Market) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Market object, thanks to `supports()`
        /** @var Market $market */
        $market = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($market, $user);
            case self::EDIT:
                return $this->canEdit($market, $user);
            case self::CLOSE:
                return $this->canClose($market, $user);
            case self::DELETE:
                return $this->canDelete($market, $user);
            case self::BUY:
                return $this->canBuy($market, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Market $market, User $user)
    {
        return $user === $market->getOwner() || $this->security->isGranted('ROLE_ADMIN');
    }

    private function canEdit(Market $market, User $user)
    {
        return $this->canView($market, $user);
    }

    private function canClose(Market $market, User $user)
    {
        return $this->canView($market, $user);
    }

    private function canDelete(Market $market, User $user)
    {
        return $this->canView($market, $user) && count($market->getItemCommands()) === 0;
    }

    private function canBuy(Market $market, User $user) {
        return $user !== $market->getOwner();
    }
}