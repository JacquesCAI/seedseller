<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ResetPasswordService {

    public static function generateRandomPassword(){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$%!?*";
        $randomPassword = "";

        for ($i=0;$i<25;$i++) {
            $randomPassword .= $chars[rand(0,strlen($chars)-1)];
        }

        return $randomPassword;
    }
}