<?php

namespace App\Custom\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

class CheckAddress extends Constraint
{
    public $message = 'Please create an address.';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }
}