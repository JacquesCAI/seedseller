const trigger = document.getElementById('moreOffersTrigger');
const content = document.getElementById('moreOffersContent');

const moreOffers = () => {
    content.className = "tw-flex tw-justify-center tw-flex-wrap";
    trigger.innerHTML = "Moins d'offres <i class=\"fas fa-chevron-up\"></i>";
    trigger.setAttribute('data-deploy', "true");
}

const lessOffers = () => {
    content.className = "tw-hidden";
    trigger.innerHTML = "Toutes mes offres <i class=\"fas fa-chevron-down\"></i>";
    trigger.setAttribute('data-deploy', "false");
}

const toogle = () => {
    let deployed = trigger.getAttribute('data-deploy');
    if(deployed === "true") {
        lessOffers();
    } else {
        moreOffers();
    }
}

trigger.addEventListener('click', event => toogle());