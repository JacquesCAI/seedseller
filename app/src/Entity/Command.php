<?php

namespace App\Entity;

use App\Repository\CommandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandRepository::class)
 */
class Command
{

    const PENDING_STATUS = "pending";
    const PAID_STATUS = "paid";
    const DISPATCHED_STATUS = "dispatched";
    const DELIVERED_STATUS = "delivered";


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Addresses::class, inversedBy="commands")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="commands")
     */
    private $owner;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deliveredAt;

    /**
     * @ORM\OneToMany(targetEntity=ItemCommand::class, mappedBy="command")
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $commandItems;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $delivery_man;

    public function __construct()
    {
        $this->commandItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?Addresses
    {
        return $this->address;
    }

    public function setAddress(?Addresses $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDeliveredAt(): ?\DateTimeInterface
    {
        return $this->deliveredAt;
    }

    public function setDeliveredAt(?\DateTimeInterface $deliveredAt): self
    {
        $this->deliveredAt = $deliveredAt;

        return $this;
    }

    public function getTotalPrice() {
        $totalPrice = 0;
        foreach ($this->getCommandItems() as $item)
            $totalPrice += $item->getPrice();
        return round($totalPrice,2);
    }

    /**
     * @return Collection|ItemCommand[]
     */
    public function getCommandItems(): Collection
    {
        return $this->commandItems;
    }

    public function addCommandItem(ItemCommand $commandItem): self
    {
        if (!$this->commandItems->contains($commandItem)) {
            $this->commandItems[] = $commandItem;
            $commandItem->setCommand($this);
        }

        return $this;
    }

    public function removeCommandItem(ItemCommand $commandItem): self
    {
        if ($this->commandItems->removeElement($commandItem)) {
            // set the owning side to null (unless already changed)
            if ($commandItem->getCommand() === $this) {
                $commandItem->setCommand(null);
            }
        }

        return $this;
    }

    public function getDeliveryMan(): ?User
    {
        return $this->delivery_man;
    }

    public function setDeliveryMan(?User $delivery_man): self
    {
        $this->delivery_man = $delivery_man;

        return $this;
    }
}
