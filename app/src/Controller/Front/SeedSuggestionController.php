<?php

namespace App\Controller\Front;

use App\Entity\Seed;
use App\Entity\SeedSuggestion;
use App\Form\SeedSuggestionType;
use App\Service\MailService;
use App\Repository\SeedSuggestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Security\Voter\SeedSuggestionVoter;

/**
 * @Route("/seed/suggestion")
 */
class SeedSuggestionController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/all", name="seed_suggestion_index", methods={"GET"})
     * @param SeedSuggestionRepository $seedSuggestionRepository
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('front/seed_suggestion/index.html.twig', [
            'seed_suggestions' => [],
            'backoffice' => false
        ]);
    }

    /**
     * @Route("/thank-you-seed-suggestion", name="seed_suggestion_thank_you")
     * @return Response
     */
    public function thankYouSeedSuggestion(): Response
    {
        return $this->render('front/seed_suggestion/thank_you_seed_suggestion.html.twig');
    }

    /**
     * @Route("/new", name="seed_suggestion_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request, MailService $mailer): Response
    {
        $user = $this->security->getUser();
        $seedSuggestion = new SeedSuggestion();
        $seedSuggestion->setSuggestorFirstname($user->getFirstname())
            ->setSuggestionLastname($user->getLastname())
            ->setSuggestorEmail($user->getEmail())
        ;
        $form = $this->createForm(SeedSuggestionType::class, $seedSuggestion);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($seedSuggestion);
            $entityManager->flush();
            $mail = $this->getUser()->getEmail();

            $mailer->sendTemplateMail($mail, 'Votre suggestion de graine', 'seed_suggestion.html.twig');

            return $this->redirectToRoute('seed_suggestion_thank_you');
        }

        return $this->render('front/seed_suggestion/new.html.twig', [
            'seed_suggestion' => $seedSuggestion,
            'form' => $form->createView(),
            'backoffice' => false
        ]);
    }

    /**
     * @Route("/{id}/edit", name="seed_suggestion_edit", methods={"GET","POST"}, requirements={"id":"\d+"})
     * @IsGranted(SeedSuggestionVoter::EDIT, subject="seedSuggestion")
     * @param Request $request
     * @param SeedSuggestion $seedSuggestion
     * @return Response
     */
    public function edit(Request $request, SeedSuggestion $seedSuggestion): Response
    {
        $form = $this->createForm(SeedSuggestionType::class, $seedSuggestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($seedSuggestion);
            $entityManager->flush();

            return $this->redirectToRoute('seed_suggestion_index');
        }

        return $this->render('front/seed_suggestion/edit.html.twig', [
            'seed_suggestion' => $seedSuggestion,
            'form' => $form->createView(),
            'backoffice' => false
        ]);
    }

}
