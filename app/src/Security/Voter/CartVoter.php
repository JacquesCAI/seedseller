<?php

namespace App\Security\Voter;

use App\Entity\Command;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

use App\Entity\ItemCommand;
use App\Entity\User;

class CartVoter extends Voter {

    const VIEW = 'view';
    const DELETE = 'delete';
    const EDIT = "edit";

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject)
    {
        if (!in_array($attribute, [self::VIEW, self::DELETE, self::EDIT])) {
            return false;
        }

        // only vote on `Command` objects
        if (!$subject instanceof ItemCommand) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a ItemCommand object, thanks to `supports()`
        /** @var ItemCommand $itemCommand */
        $itemCommand = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($itemCommand, $user);
            case self::DELETE:
                return $this->canDeleteOrEdit($itemCommand, $user);
            case self::EDIT:
                return $this->canDeleteOrEdit($itemCommand, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(ItemCommand $itemCommand, User $user)
    {
        return $user === $itemCommand->getCommand()->getOwner();
    }

    private function canDeleteOrEdit(ItemCommand $itemCommand, User $user) {
        return $this->canView($itemCommand,$user) && $itemCommand->getCommand()->getStatus() == Command::PENDING_STATUS;
    }
}