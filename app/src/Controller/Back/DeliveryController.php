<?php

namespace App\Controller\Back;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Security\Voter\UserVoter;
use App\Entity\User;
use App\Service\MailService;

class DeliveryController extends AbstractController
{
    /**
     * @Route("/delivery", name="delivery")
     */
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findBy(["request_delivery" => 2]);

        return $this->render('back/delivery/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/{id}/validate_request_delivery", name="validate_request_delivery", methods={"POST"}, requirements={"id":"\d+"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function validate_request_delivery(Request $request, User $user, MailService $mailer): Response
    {

        if ($this->isCsrfTokenValid('validate_request_delivery' . $user->getId(), $request->request->get('_token')) && $user->getrequest_delivery() == 2) {

            $user->setrequest_delivery(1);
            $user->addRole('ROLE_DELIVERY');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $mail = $user->getEmail();
            $mailer->sendTemplateMail($mail, 'Votre demande pour devenir livreur', 'accept_delivery_guy.html.twig');
            $this->addFlash('success', 'L\'utilisateur a bien été ajouté aux livreurs');
        }

        return $this->redirectToRoute('admin_delivery');
    }

    /**
     * @Route("/{id}/refused_request_delivery", name="refused_request_delivery", methods={"POST"}, requirements={"id":"\d+"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function refused_request_delivery(Request $request, User $user, MailService $mailer): Response
    {

        if ($this->isCsrfTokenValid('refused_request_delivery' . $user->getId(), $request->request->get('_token')) && $user->getrequest_delivery() == 2) {

            $user->setrequest_delivery(0);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $mail = $user->getEmail();
            $mailer->sendTemplateMail($mail, 'Votre demande pour devenir livreur', 'deny_delivery_guy.html.twig');
            $this->addFlash('success', 'La demande a bien été refusé!');
        }

        return $this->redirectToRoute('admin_delivery');
    }
}
