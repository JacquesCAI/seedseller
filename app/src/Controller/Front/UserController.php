<?php

namespace App\Controller\Front;

use App\Form\ChangePasswordType;
use App\Form\UserType;
use App\Form\ChangeProfilePicType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Security\Voter\UserVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

/**
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="crud", methods={"GET","POST"})
     */
    public function edit(Request $request): Response {
        $user = $this->getUser();
        $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        $form_password = $this->createForm(ChangePasswordType::class, $user);
        $form_password->handleRequest($request);

        $form_profile_pic = $this->createForm(
            ChangeProfilePicType::class,
            $user
        );
        $form_profile_pic->handleRequest($request);

        $oneSubmitted = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Vos informations personnelles ont bien été modifié!');
            $oneSubmitted = true;
        }
        if ($form_profile_pic->isSubmitted() && $form_profile_pic->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $this->addFlash('success', 'Votre photo de profil a bien été modifié!');
            $oneSubmitted = true;
        }
        if ($form_password->isSubmitted() && $form_password->isValid()) {
            $user->setPlainPassword($form_password->get('password')->getData());
            $user->setPassword($form_password->get('password')->getData());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $this->addFlash('success', "Votre mot de passe a bien été modifié!");
            $oneSubmitted = true;
        }
        if ($oneSubmitted) {
            $this->getDoctrine()
                ->getManager()
                ->flush();
            return $this->redirectToRoute('user_crud');
        }
        return $this->render('front/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'form_password' => $form_password->createView(),
            'form_profile_pic' => $form_profile_pic->createView(),
        ]);
    }

    /**
     * @Route("/request_delivery/{id}", name="request_delivery", methods={"POST"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function requestDelivery(Request $request, User $user): Response
    {
        if (
            $this->isCsrfTokenValid(
                'request_delivery' . $user->getId(),
                $request->request->get('_token')
            ) &&
            $user->getrequest_delivery() == 0
        ) {
            $user->setrequest_delivery(2);

            $userManager = $this->getDoctrine()->getManager();
            $userManager->persist($user);
            $userManager->flush();
            $this->addFlash('success', 'Votre demande a bien été enregistré!');
        }

        return $this->redirectToRoute('user_crud');
    }
}
