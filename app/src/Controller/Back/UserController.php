<?php

namespace App\Controller\Back;

use App\Entity\Command;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

/**
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET","POST"})
     */
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();

        return $this->render('back/user/index.html.twig', [
            'users' => $users
        ]);
    }
    /**
     * @Route("/search", name="search", methods={"GET","POST"})
     */
    public function search(Request $request, UserRepository $userRepository) {
        $body = json_decode($request->getContent());

        $data = $userRepository->findBySearchAndRole($body->search, $body->role);
        return new Response(json_encode($data), Response::HTTP_OK);
    }
    /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     * @param User $user
     */
    public function show(User $user, Request $request) {
        return $this->render('back/user/show.html.twig', [
            'user' => $user,
            'referer' => $request->headers->get('referer')
        ]);
    }

    /**
     * @Route("/search_request_delivery", name="search_request_delivery", methods={"GET","POST"})
     */
    public function searchRequestDelivery(Request $request, UserRepository $userRepository) {
        $body = json_decode($request->getContent());

        $data = $userRepository->findByRequestDeliveryman($body->search);
        return new Response(json_encode($data), Response::HTTP_OK);
    }

    /**
     * @Route("/{id}/remove_delivery_role", name="remove_delivery_role", methods={"POST"}, requirements={"id":"\d+"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function remove_delivery_role(Request $request, User $user): Response
    {

        if ($this->isCsrfTokenValid('remove_delivery_role' . $user->getId(), $request->request->get('_token')) && $user->getrequest_delivery() == 1) {
            foreach ($user->getCommandsToDelivery() as $command) {
                $command->setDeliveryMan(null);
                $command->setStatus(Command::PAID_STATUS);
            }
            $user->setrequest_delivery(0);
            $user->removeRole('ROLE_DELIVERY');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'L\'utilisateur `'. $user->getFirstname() .' '. $user->getLastname() .'` a bien été retiré des livreurs!');
        }

        return $this->redirectToRoute('admin_user_show', ['id' => $user->getId()]);
    }
}


