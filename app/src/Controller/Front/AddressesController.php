<?php

namespace App\Controller\Front;

use App\Entity\Addresses;
use App\Form\AddressesType;
use App\Repository\AddressesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Security\Voter\AddressesVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/addresses")
 */
class AddressesController extends AbstractController
{
    /**
     * @Route("/", name="addresses_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('front/addresses/index.html.twig', [
            'addresses' => $this->getUser()->getAddresses(),
        ]);
    }

    /**
     * @Route("/new", name="addresses_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $address = new Addresses();
        $form = $this->createForm(AddressesType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $address->setCustomer($this->getUser());
            $entityManager->persist($address);
            $entityManager->flush();
            $redirectTo = $request->get("redirectTo");
            if ($redirectTo) {
                return $this->redirect($redirectTo);
            }
            $this->addFlash('success', 'Votre adresse a été ajouté avec succès!');
            return $this->redirectToRoute('addresses_index');
        }

        return $this->render('front/addresses/new.html.twig', [
            'address' => $address,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="addresses_show", methods={"GET"})
     */
    public function show(Addresses $address): Response
    {
        if ($address->getCustomer()->getId() != $this->getUser()->getId()) {
            return $this->redirectToRoute('addresses_index');
        }
        return $this->render('front/addresses/show.html.twig', [
            'address' => $address,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="addresses_edit", methods={"GET","POST"})
     * @IsGranted(AddressesVoter::EDIT, subject="address")
     */
    public function edit(Request $request, Addresses $address): Response
    {
        if ($address->getCustomer()->getId() != $this->getUser()->getId()) {
            return $this->redirectToRoute('addresses_index');
        }

        $form = $this->createForm(AddressesType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre adresse a bien été modifié!');
            return $this->redirectToRoute('addresses_index');
        }

        return $this->render('front/addresses/edit.html.twig', [
            'address' => $address,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="addresses_delete", methods={"DELETE"})
     * @IsGranted(AddressesVoter::DELETE, subject="address")
     */
    public function delete(Request $request, Addresses $address): Response
    {
        if ($address->getCustomer()->getId() != $this->getUser()->getId()) {
            return $this->redirectToRoute('addresses_index');
        }
        if ($this->isCsrfTokenValid('delete'.$address->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($address);
            $entityManager->flush();
        }

        return $this->redirectToRoute('addresses_index');
    }
}
