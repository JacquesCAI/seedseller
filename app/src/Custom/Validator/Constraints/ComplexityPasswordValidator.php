<?php
namespace App\Custom\Validator\Constraints;

use Symfony\Component\Validator\Constraint,
    Symfony\Component\Validator\ConstraintValidator;

//use Doctrine\ORM\EntityManager;

class ComplexityPasswordValidator extends ConstraintValidator {

//    protected $em;
//
//    public function __construct(EntityManager $entityManager) {
//        $this->em = $entityManager;
//    }

    public function validate($value, Constraint $constraint) {
        $password = $this->context->getRoot()->get('password')->getData();

        $specialChars = ["@","[","]","^","_","!","\"","#","$","%","&","'","(",")","*","+","-",".","/",":",";","{","}","<",">","=","|","~","?"];

        $maj = false;
        $min = false;
        $number = false;
        $special = false;
        for ($i=0;$i<strlen($password) && (!$maj || !$min || !$number || !$special);$i++) {
            if (in_array($password[$i],$specialChars))
                $special = true;
            else if (is_numeric($password[$i]))
                $number = true;
            else if (strtolower($password[$i]) != $password[$i])
                $maj = true;
            else if (strtoupper($password[$i]) != $password[$i])
                $min = true;
        }
        if(!$maj || !$min || !$number || !$special) {
            $this->context->buildViolation($constraint->message)
                ->atPath('password')
                ->addViolation();
        }
    }

}